/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef DISPLAYHANDLER_H
#define DISPLAYHANDLER_H

#include <QMap>
#include <QPointer>
#include <QStringList>

class DisplayHandler
{
public:
    DisplayHandler() = default;
    ~DisplayHandler();

    static QString writeTextOutput(const QList<class Asset *> &picked, int players, int failedTries, double seconds);

    void addList(QList<class Asset*> picked, double seconds, int failedTries);
    void clearLists();
    int count() const;
    QString writeLists(const QString &order = QStringLiteral("No List Order")) const;

    bool plotStatistics(bool totOnly, const QList<class Asset *> &other = QList<class Asset *>(), const QString &filename = QStringLiteral("statistics.png")) const;//draws a primitive bar diagram to see how many of each asset were distributed
private:
    QMap<QString, QMap<QString, int> > gatherStatistics(const QList<class ResultList> &l) const;//counts occurences of all assets in the result-lists

    QList<class ResultList> lists;
};

//_________________________________________________________

class ResultList
{
public:
    ResultList(const QList<QPointer<class Asset> > &Results, double Tonnage, double BV, double Cost, double Seconds, int FailedTries)
        : tonnage(Tonnage), bv(BV), cost(Cost), seconds(Seconds), failedTries(FailedTries), results(Results) {}

    static void setOrder(const QString &order)
    {
        //ordering possibilities:
        //"No List Order"
        //"Mass->BV->CBills"
        //"Mass->CBills->BV"
        //"BV->Mass->CBills"
        //"BV->CBills->Mass"
        //"CBills->Mass->BV"
        //"CBills->BV->Mass"
        QStringList o = order.split(QStringLiteral("\u2b02"));
        if(o.count() != 3)
            ResultList::orderBy.clear();
        else
            ResultList::orderBy = o;
    }

    bool operator<(const ResultList &other) const
    {
        if(ResultList::orderBy.isEmpty())
        {
            return(true);//should keep whatever order there currently is
        }
        if(ResultList::orderBy == QStringList() << QStringLiteral("Mass") << QStringLiteral("BV") << QStringLiteral("CBills"))
            return(this->tonnage < other.getTonnage()
                   || (!(other.getTonnage() < this->tonnage) && this->bv < other.getBv())
                   || (!(other.getTonnage() < this->tonnage) && !(other.getBv() < this->bv) && this->cost < other.getCost()));
        else if(ResultList::orderBy == QStringList() << QStringLiteral("Mass") << QStringLiteral("CBills") << QStringLiteral("BV"))
            return(this->tonnage < other.getTonnage()
                   || (!(other.getTonnage() < this->tonnage) && this->cost < other.getCost())
                   || (!(other.getTonnage() < this->tonnage) && !(other.getCost() < this->cost) && this->bv < other.getBv()));
        else if(ResultList::orderBy == QStringList() << QStringLiteral("BV") << QStringLiteral("Mass") << QStringLiteral("CBills"))
            return(this->bv < other.getBv()
                   || (!(other.getBv() < this->bv) && this->tonnage < other.getTonnage())
                   || (!(other.getBv() < this->bv) && !(other.getTonnage() < this->tonnage) && this->cost < other.getCost()));
        else if(ResultList::orderBy == QStringList() << QStringLiteral("BV") << QStringLiteral("CBills") << QStringLiteral("Mass"))
            return(this->bv < other.getBv()
                   || (!(other.getBv() < this->bv) && this->cost < other.getCost())
                   || (!(other.getBv() < this->bv) && !(other.getCost() < this->cost) && this->tonnage < other.getTonnage()));
        else if(ResultList::orderBy == QStringList() << QStringLiteral("CBills") << QStringLiteral("Mass") << QStringLiteral("BV"))
            return(this->cost < other.getCost()
                   || (!(other.getCost() < this->cost) && this->tonnage < other.getTonnage())
                   || (!(other.getCost() < this->cost) && !(other.getTonnage() < this->tonnage) && this->bv < other.getBv()));
        else// if(ResultList::orderBy == QStringList() << QStringLiteral("CBills") << QStringLiteral("BV") << QStringLiteral("Mass"))
            return(this->cost < other.getCost()
                   || (!(other.getCost() < this->cost) && this->bv < other.getBv())
                   || (!(other.getCost() < this->cost) && !(other.getBv() < this->bv) && this->tonnage < other.getTonnage()));
    }

    double getTonnage() const  { return(this->tonnage); }
    double getBv() const { return(this->bv); }
    double getCost() const  { return(this->cost); }

    int getFailedTries() const { return(this->failedTries); }
    QList<QPointer<class Asset> > getResults() const { return(this->results); }
    double getSeconds() const { return(this->seconds); }
private:
    double tonnage;
    double bv;
    double cost;

    double seconds;
    int failedTries;

    static QStringList orderBy;

    QList<QPointer<class Asset> > results;
};

#endif // DISPLAYHANDLER_H
