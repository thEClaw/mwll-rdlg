﻿Developed using Qt by Digia: www.qt.io

Using icons from this free icon set: Free FatCow-Farm Fresh Icons, http://www.fatcow.com/free-icons

Applying the random number generators by Agner Fog: www.agner.org/random/.

---------------------

This program is distributed under the terms of the GNU Lesser General Public License.
http://www.gnu.org/licenses/lgpl-3.0.txt