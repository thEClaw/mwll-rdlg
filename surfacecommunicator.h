/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef SURFACECOMMUNICATOR_H
#define SURFACECOMMUNICATOR_H

#include <QObject>

class SurfaceCommunicator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool copyResultsToClipboard READ getCopyResultsToClipboard WRITE setCopyResultsToClipboard NOTIFY copyResultsToClipboardChanged)
    Q_PROPERTY(bool createStatisticalOverview READ getCreateStatisticalOverview WRITE setCreateStatisticalOverview NOTIFY createStatisticalOverviewChanged)
    Q_PROPERTY(bool noOutput READ getNoOutput WRITE setNoOutput NOTIFY noOutputChanged)
    Q_PROPERTY(double gaussianWidth READ getGaussianWidth WRITE setGaussianWidth NOTIFY gaussianWidthChanged)
    Q_PROPERTY(unsigned int lists READ getLists WRITE setLists NOTIFY listsChanged)
    Q_PROPERTY(unsigned short players READ getPlayers WRITE setPlayers NOTIFY playersChanged)
    Q_PROPERTY(QString sortBy READ getSortBy WRITE setSortBy NOTIFY sortByChanged)
    Q_PROPERTY(QString orderListsBy READ getOrderListsBy WRITE setOrderListsBy NOTIFY orderListsByChanged)
public:
    explicit SurfaceCommunicator(QObject *parent = nullptr);

    bool getCopyResultsToClipboard() const;
    bool getCreateStatisticalOverview() const;
    double getGaussianWidth() const;
    unsigned int getLists() const;
    bool getNoOutput() const;
    QString getOrderListsBy() const;
    unsigned short getPlayers() const;
    QString getSortBy() const;

    bool Load(const QString &file);
    bool Save(const QString &file) const;
public slots:
    void setCopyResultsToClipboard(bool c);
    void setCreateStatisticalOverview(bool s);
    void setGaussianWidth(double g);
    void setLists(unsigned int l);
    void setNoOutput(bool o);
    void setOrderListsBy(const QString &o);
    void setPlayers(unsigned short p);
    void setSortBy(const QString &s);
signals:
    void copyResultsToClipboardChanged(bool cp);
    void createStatisticalOverviewChanged(bool st);
    void gaussianWidthChanged(double gw);
    void listsChanged(int lc);
    void noOutputChanged(bool o);
    void orderListsByChanged(QString ob);
    void playersChanged(unsigned short pc);
    void sortByChanged(QString sb);
private:
    bool copyResultsToClipboard;
    bool createStatisticalOverview;
    bool noOutput;
    double gaussianWidth;
    unsigned int lists;
    unsigned short players;
    QString sortBy;
    QString orderListsBy;
};

#endif // SURFACECOMMUNICATOR_H
