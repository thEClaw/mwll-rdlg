/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef ASSET_H
#define ASSET_H

#include <QMap>
#include <QObject>
#include <QVariant>

class Asset : public QObject
{
    Q_OBJECT
public:
    explicit Asset(const QString &name, const QMultiMap<QString, QVariant> &data, QObject *parent = nullptr); //data is read in somewhere else and then saved as a dynamic property here

    Asset(const Asset &other) = delete;
    Asset &operator=(const Asset &other) = delete;

    ~Asset() = default;

    void setValue(const QString &name, const QVariant &value);

    QMultiMap<QString, QVariant> getData() const;

    //convenience methods
    double getMass();
    inline const QString getName() const { return(this->objectName()); } //use the objectName() to store this value
    double getPrice();

    bool operator==(const Asset &other) const;
private:
    void setData(const QMultiMap<QString, QVariant> &data);

    QPair<bool, double> cached_mass;
    QPair<bool, double> cached_price;
};

#endif // ASSET_H
