/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

import QtQuick 2.4
import QtQuick.Controls 1.3

Rectangle {
    Connections {
            target: communicator
            //onCopyResultsToClipboardChanged: copyToClipboard.checked = cp
            onCreateStatisticalOverviewChanged: statisticalOverview.checked = st
            onListsChanged: listAmount.value = lc
            onPlayersChanged: playerAmount.value = pc
            onSortByChanged: {
                if(sortBy.find(sb) < 0)
                    sortItems.append( {"text": sb} )
                sortBy.currentIndex = sortBy.find(sb)
            }
            onOrderListsByChanged: {
                if(orderListsBy.find(ob) < 0)
                    orderListsBy.currentIndex = 0
                else
                    orderListsBy.currentIndex = orderListsBy.find(ob)
            }
    }

    color: "#00000000"
    anchors.bottom: buttonGenerate.bottom
    anchors.bottomMargin: 0
    anchors.top: parent.top
    anchors.topMargin: 5
    anchors.right: parent.right
    anchors.rightMargin: 15
    anchors.left: buttonGenerate.right
    anchors.leftMargin: 6

    SpinBox {
        id: playerAmount
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: listAmount.left
        anchors.rightMargin: 6
        value: communicator.players
        maximumValue: 64
        minimumValue: 1
        suffix: " Players"
        onValueChanged: {
            communicator.setPlayers(playerAmount.value)
        }
    }

    SpinBox {
        id: listAmount
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: sortBy.left
        anchors.rightMargin: 6
        value: communicator.lists
        suffix: " Lists"
        maximumValue: 5000
        minimumValue: 1
        onValueChanged: {
            communicator.setLists(listAmount.value)
        }
    }

    ComboBox {
        id: sortBy
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: orderListsBy.left
        anchors.rightMargin: 6
        editable: false
        model: ListModel {
            id: sortItems
            ListElement { text: "BattleValue" }
            ListElement { text: "Faction"     }
            ListElement { text: "Name"        }
            ListElement { text: "Mass"        }
            ListElement { text: "Price"       }
            ListElement { text: "Type"        }
        }
        currentIndex: {
            if(find(communicator.sortBy) < 0)
                2
            else
                find(communicator.sortBy)
        }
        onActivated: {
            communicator.setSortBy(sortBy.textAt(index))
        }
    }

    ComboBox {
        id: orderListsBy
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        editable: false
        model: ListModel {
            id: orderItems
            ListElement { text: "No List Order"   }
            ListElement { text: "Mass\u2b02BV\u2b02CBills" }
            ListElement { text: "Mass\u2b02CBills\u2b02BV" }
            ListElement { text: "BV\u2b02Mass\u2b02CBills" }
            ListElement { text: "BV\u2b02CBills\u2b02Mass" }
            ListElement { text: "CBills\u2b02Mass\u2b02BV" }
            ListElement { text: "CBills\u2b02BV\u2b02Mass" }
        }
        currentIndex: {
            if(find(communicator.orderListsBy) < 0)
                0
            else
                find(communicator.orderListsBy)
        }
        onActivated: {
            communicator.setOrderListsBy(orderListsBy.textAt(index))
        }
    }

//    CheckBox {
//        id: copyToClipboard
//        height: 22
//        text: qsTr("Copy To Clipboard")
//        anchors.top: parent.top
//        anchors.topMargin: 2
//        anchors.right: statisticalOverview.left
//        anchors.rightMargin: 12
//        checked: communicator.copyResultsToClipboard
//        onClicked: {
//            communicator.setCopyResultsToClipboard(copyToClipboard.checked)
//        }
//    }

    CheckBox {
        id: statisticalOverview
        height: 22
        text: qsTr("Statistical Overview")
        anchors.top: parent.top
        anchors.topMargin: 2
        anchors.right: playerAmount.left
        anchors.rightMargin: 12
        checked: communicator.createStatisticalOverview
        onClicked: {
            communicator.setCreateStatisticalOverview(statisticalOverview.checked)
        }
    }
}
