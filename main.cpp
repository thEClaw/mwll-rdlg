/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef CONSOLE_ONLY
    #include <QApplication>
    #include <QQmlApplicationEngine>
    #include <QQmlContext>

    #include <QtQml>
#else
    #include <QCommandLineParser>
    #include <QCoreApplication>
    #include <QTextStream>
    #include <QTimer>
#endif

#include "criteria/abstractcriterion.h"
#include "datahandler.h"
#include "surfacecommunicator.h"

void handleParameters(const QCoreApplication &app, SurfaceCommunicator *s)
{
        QCommandLineParser parser;
        parser.setApplicationDescription(QStringLiteral("Random drop-list generator for 'MechWarrior: Living Legends'."));
        parser.addHelpOption();//accessible via -h/--help
        parser.addVersionOption();//accessible via -v/--version

        //add options
        QCommandLineOption playerOption(QStringList() << QStringLiteral("p") << QStringLiteral("players"),
                QStringLiteral("Amount of assets needed per team respectively the amount of players per team."),
                QStringLiteral("amount"));
        parser.addOption(playerOption);

        QCommandLineOption listOption(QStringList() << QStringLiteral("l") << QStringLiteral("lists"),
                QStringLiteral("Amount of lists to be generated in total."),
                QStringLiteral("lists"));
        parser.addOption(listOption);

        // Process the actual command line arguments given by the user
        parser.process(app);

        bool ok = false;
        int num = parser.value(playerOption).toInt(&ok);
        if(ok)
        {
            s->setPlayers(num);
        }
        num = parser.value(listOption).toInt(&ok);
        if(ok)
        {
            s->setLists(num);
        }
}

int main(int argc, char *argv[])
{
#ifndef CONSOLE_ONLY
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;
#else
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName(QStringLiteral("MWLLDropRand"));
    QCoreApplication::setApplicationVersion(QStringLiteral("1.5"));
#endif
    SurfaceCommunicator s;
    DataHandler d(&s);

#ifndef CONSOLE_ONLY
    engine.rootContext()->setContextProperty(QStringLiteral("handler"), &d);
    engine.rootContext()->setContextProperty(QStringLiteral("communicator"), &s);
    qmlRegisterType<AbstractCriterion>();//make d.criteria and its elements accessible via Qml

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));
#else
    handleParameters(app, &s);
    s.setNoOutput(false);//overwrite in case of commandline-only mode

    QObject::connect(&d, &DataHandler::message, [](const QString &msg){
        QTextStream(stdout) << qPrintable(msg) << QLatin1String("\n");
    });
    QObject::connect(&d, &DataHandler::listDone, [](const QString &msg){
        QTextStream(stdout) << QLatin1String("\n============================================================\n\n") << qPrintable(msg);
    });
    QObject::connect(&d, &DataHandler::allListsDone, [](){
        QTextStream(stdout) << QLatin1String("\n\n");
    });
    QObject::connect(&d, &DataHandler::allListsDone, &app, &QCoreApplication::quit);//quit the application once lists are generated
    QTimer::singleShot(0, &d, SLOT(generate()));
#endif
    return app.exec();
}
