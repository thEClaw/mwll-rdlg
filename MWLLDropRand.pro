# Copyright 2015 S. Ehlers
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# http://www.gnu.org/licenses/lgpl-3.0.txt

TEMPLATE = app

CONFIG += c++11

DEFINES += CONSOLE_ONLY #activate from commandline via: 'qmake "DEFINES += CONSOLE_ONLY"'

contains(DEFINES, CONSOLE_ONLY)
{
    TARGET = MWLLDropRand_console
    QT       += core
    QT       -= gui
    CONFIG   += console
    CONFIG   -= app_bundle
    message("Creating: Console version")
}
!contains(DEFINES, CONSOLE_ONLY){
    TARGET = MWLLDropRand
    QT += quick widgets
    RESOURCES += qml.qrc

    # Additional import path used to resolve QML modules in Qt Creator's code model
    QML_IMPORT_PATH =
    message("Creating: GUI version")
}

SOURCES += main.cpp \
    criteria/abstractcriterion.cpp \
    asset.cpp \
    criteria/budget.cpp \
    criteria/checklist.cpp \
    datahandler.cpp \
    rng.cpp \
    rng/randomc/mersenne.cpp \
    rng/randomc/userintf.cpp \
    rng/stocc/stoc1.cpp \
    surfacecommunicator.cpp \
    criteria/repetitions.cpp \
    displayhandler.cpp

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    criteria/abstractcriterion.h \
    asset.h \
    criteria/budget.h \
    criteria/checklist.h \
    datahandler.h \
    rng.h \
    rng/randomc/randomc.h \
    rng/stocc/stocc.h \
    surfacecommunicator.h \
    criteria/repetitions.h \
    displayhandler.h
