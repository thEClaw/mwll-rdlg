/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

import QtQuick 2.4
import QtQuick.Controls 2.0
//import QtQuick.Controls.Universal 2.12

Button {
    text: qsTr("Go!")
    anchors.left: parent.left
    anchors.leftMargin: 15
    anchors.top: parent.top
    anchors.topMargin: 5

    style: ButtonStyle {
        background: Rectangle {
            implicitWidth: 100
            implicitHeight: 25
            border.width: control.activeFocus ? 2 : 1
            border.color: "#888"
            radius: 15
            gradient: Gradient {
                GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
            }
        }
    }

    onClicked: {
        if(state == "generating") {
            handler.cancelGeneration = true
            buttonGenerate.state = "idle"
        }
        else if(state == "idle" || state == "") {
            buttonGenerate.state = "generating"
            handler.cancelGeneration = false
            handler.generate()
        }
    }
    states: [
        State {
            name: "idle"
            PropertyChanges { target: buttonGenerate; text: qsTr("Go!") }
            PropertyChanges { target: settingContainer; enabled: true }
            PropertyChanges { target: listViewContainer; enabled: true }
            PropertyChanges { target: fileMenu; enabled: true }
        },
        State {
            name: "generating"
            PropertyChanges { target: buttonGenerate; text: qsTr("Stop!") }
            PropertyChanges { target: settingContainer; enabled: false }
            PropertyChanges { target: listViewContainer; enabled: false }
            PropertyChanges { target: fileMenu; enabled: false }
        }
    ]
    onStateChanged: {
        if(state == "idle")
            applicationWindow.alert(5000)
    }
}
