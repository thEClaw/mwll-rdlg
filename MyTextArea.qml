/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

import QtQuick 2.12
import QtQuick.Controls 2.12

TextArea {
    readOnly: true
    frameVisible: false
    state: "empty"
    font.family: "Courier"
    wrapMode: TextEdit.NoWrap

    states: [
            State {
                name: "empty"
                PropertyChanges {
                    target: areaDismiss
                    visible: false
                }
                PropertyChanges {
                    target: areaCopy
                    visible: false
                }
                PropertyChanges {
                    target: areaSave
                    visible: false
                }
            },
            State {
                name: "newText"
                PropertyChanges {
                    target: areaDismiss
                    visible: true
                }
                PropertyChanges {
                    target: areaCopy
                    visible: true
                }
                PropertyChanges {
                    target: areaSave
                    visible: true
                }
            },
            State {
                name: "saved"
                PropertyChanges {
                    target: areaDismiss
                    visible: true
                }
                PropertyChanges {
                    target: areaCopy
                    visible: true
                }
                PropertyChanges {
                    target: areaSave
                    visible: false
            }
        }
    ]

    Rectangle {
        id: areaDismiss
        width: 26
        height: 26
        color: "transparent"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3

        Image {
            id: iconDismiss
            anchors.fill: parent
            anchors.margins: 3
            source: "icons/clear.png"
        }

        MouseArea {
            id: mouseDismiss
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton
            hoverEnabled: true
            onClicked: {
                droplist.text = ""
                droplist.state = "empty"
            }
            onEntered: parent.color = "#50ace4"
            onExited: parent.color = "transparent"
        }
    }

    Rectangle {
        id: areaSave
        width: 26
        height: 26
        color: "transparent"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 3
        anchors.right: parent.right
        anchors.rightMargin: 20

        Image {
            id: iconSave
            anchors.fill: parent
            anchors.margins: 3
            source: "icons/save.png"
        }

        MouseArea {
            id: mouseSave
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton
            hoverEnabled: true
            onClicked: {
                handler.saveLists(droplist.text) //saves things multiple times if pressed multiple times...not a good way
                droplist.state = "saved"
            }
            onEntered: parent.color = "#50ace4"
            onExited: parent.color = "transparent"
        }
    }

    Rectangle {
        id: areaCopy
        width: 26
        height: 26
        color: "transparent"
        anchors.right: areaSave.left
        anchors.rightMargin: 3
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 3

        Image {
            id: iconCopy
            anchors.fill: parent
            anchors.margins: 3
            source: "icons/copy.png"
        }

        MouseArea {
            id: mouseCopy
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton
            hoverEnabled: true
            onClicked: {
                handler.copyToClipboard(droplist.text);
            }
            onEntered: parent.color = "#50ace4"
            onExited: parent.color = "transparent"
        }
    }
}

