/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#include <QRegularExpression>

#include "asset.h"

Asset::Asset(const QString &name, const QMultiMap<QString, QVariant> &data, QObject *parent) :
    QObject(parent), cached_mass(false, 0), cached_price(false, 0)
{
    this->setData(data);
    this->setObjectName(name);
}

double Asset::getMass()
{
    if(cached_mass.first)
        return(cached_mass.second);
    QVariant v = this->property("mass");
    if(v.isValid() && v.canConvert<double>())
    {
        bool ok;
        double vv = v.toDouble(&ok);
        if(ok)
        {
            cached_mass.second = vv;
            cached_mass.first = true;
            return(vv);
        }
    }
    return(0);
}

double Asset::getPrice()
{
    if(cached_price.first)
        return(cached_price.second);
    QVariant v = this->property("price");
    if(v.isValid() && v.canConvert<double>())
    {
        bool ok;
        double vv = v.toDouble(&ok);
        if(ok)
        {
            cached_price.second = vv;
            cached_price.first = true;
            return(vv);
        }
    }
    return(0);
}

void Asset::setValue(const QString &name, const QVariant &value)
{
    cached_mass.first = false;
    cached_price.first = false;
    this->setProperty(name.toUtf8().constData(), value);
}

QMultiMap<QString, QVariant> Asset::getData() const
{
    QMultiMap<QString, QVariant> m;
    QList<QByteArray> properties = this->dynamicPropertyNames();
    for(auto i = properties.constBegin(); i != properties.constEnd(); ++i)
    {
        m.insert(*i, this->property(i->constData()));
    }
    return(m);
}

void Asset::setData(const QMultiMap<QString, QVariant> &data)
{
    cached_mass.first = false;
    cached_price.first = false;
    for(auto i = data.constBegin(); i != data.constEnd() ; ++i)
    {
        this->setProperty(i.key().toUtf8().constData(), i.value());
    }
}

bool Asset::operator ==(const Asset &other) const
{
    if(this == &other)
    {
        return(true);
    }

    if(this->objectName().compare(other.objectName()) == 0)
    {
        QList<QByteArray> properties = this->dynamicPropertyNames();
        for(auto i = properties.constBegin(); i != properties.constEnd(); ++i)
        {
            if(this->property(i->constData()) != other.property(i->constData()))
            {
                return(false);
            }
        }
    }
    return(true);
}
