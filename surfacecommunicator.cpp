/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#include <QFile>
#include <QSettings>

#include "surfacecommunicator.h"

SurfaceCommunicator::SurfaceCommunicator(QObject *parent)
    : QObject(parent), copyResultsToClipboard(false), createStatisticalOverview(false), noOutput(false), gaussianWidth(0.4),
      lists(1), players(8), sortBy(QLatin1String("mass")), orderListsBy(QLatin1String("No List Order"))
{
}

bool SurfaceCommunicator::Load(const QString &file)
{
    if(!QFile::exists(file))
    {
        return(false);
    }

    QSettings s(file, QSettings::IniFormat);

    copyResultsToClipboard = (s.value(QStringLiteral("copyResultsToClipboard"), QStringLiteral("false")).toString() == QLatin1String("true"));
    createStatisticalOverview = (s.value(QStringLiteral("createStatisticalOverview"), QStringLiteral("false")) == QLatin1String("true"));
    //gaussianWidth = qMax(0.0, s.value(QStringLiteral("gaussianWidth"), 0.4).toDouble());/*-----reenable in some later version (disabled in 1.3 to enforce a new value w/o confusing the user*/
    lists = s.value(QStringLiteral("lists"), 1).toUInt();
    noOutput = (s.value(QStringLiteral("noOutput"), QStringLiteral("false")).toString() == QLatin1String("true"));
    orderListsBy = s.value(QStringLiteral("orderListsBy"), QStringLiteral("No List Order")).toString().replace(QStringLiteral("->"), QStringLiteral("\u2b02"));
    players = s.value(QStringLiteral("players"), 8).toUInt();
    sortBy = s.value(QStringLiteral("sortBy"), QStringLiteral("mass")).toString();

    emit copyResultsToClipboardChanged(copyResultsToClipboard);
    emit createStatisticalOverviewChanged(createStatisticalOverview);
    emit gaussianWidthChanged(gaussianWidth);
    emit listsChanged(lists);
    emit noOutputChanged(noOutput);
    emit orderListsByChanged(orderListsBy);
    emit playersChanged(players);
    emit sortByChanged(sortBy);

    return(true);
}

bool SurfaceCommunicator::Save(const QString &file) const
{
    QFile::remove(file);

    QSettings s(file, QSettings::IniFormat);

    s.setValue(QStringLiteral("copyResultsToClipboard"), copyResultsToClipboard ? QStringLiteral("true") : QStringLiteral("false"));
    s.setValue(QStringLiteral("createStatisticalOverview"), createStatisticalOverview ? QStringLiteral("true") : QStringLiteral("false"));
    s.setValue(QStringLiteral("gaussianWidth"), QString::number(double(gaussianWidth), 'g', 4));
    s.setValue(QStringLiteral("lists"), lists);
    s.setValue(QStringLiteral("noOutput"), noOutput ? QStringLiteral("true") : QStringLiteral("false"));
    s.setValue(QStringLiteral("players"), players);
    s.setValue(QStringLiteral("sortBy"), sortBy);
    s.setValue(QStringLiteral("orderListsBy"), QString(orderListsBy).replace(QStringLiteral("\u2b02"), QStringLiteral("->")));

    return(true);
}

bool SurfaceCommunicator::getCopyResultsToClipboard() const
{
    return(copyResultsToClipboard);
}

void SurfaceCommunicator::setCopyResultsToClipboard(bool c)
{
    if(copyResultsToClipboard != c)
    {
        copyResultsToClipboard = c;
        emit copyResultsToClipboardChanged(copyResultsToClipboard);
    }
}

bool SurfaceCommunicator::getCreateStatisticalOverview() const
{
    return(createStatisticalOverview);
}

void SurfaceCommunicator::setCreateStatisticalOverview(bool s)
{
    if(createStatisticalOverview != s)
    {
        createStatisticalOverview = s;
        emit createStatisticalOverviewChanged(createStatisticalOverview);
    }
}

double SurfaceCommunicator::getGaussianWidth() const
{
    return(gaussianWidth);
}

void SurfaceCommunicator::setGaussianWidth(double g)
{
    if(qAbs(gaussianWidth - g) > 0.00000000001) /*-----imprecise but probably sufficient, right?*/
    {
        gaussianWidth = g;
        emit gaussianWidthChanged(gaussianWidth);
    }
}

unsigned int SurfaceCommunicator::getLists() const
{
    return(lists);
}

void SurfaceCommunicator::setLists(unsigned int l)
{
    if(lists != l)
    {
        lists = l;
        emit listsChanged(lists);
    }
}

bool SurfaceCommunicator::getNoOutput() const
{
    return(noOutput);
}

void SurfaceCommunicator::setNoOutput(bool o)
{
    if(noOutput != o)
    {
        noOutput = o;
        emit noOutputChanged(noOutput);
    }
}

QString SurfaceCommunicator::getOrderListsBy() const
{
    return(orderListsBy);
}

void SurfaceCommunicator::setOrderListsBy(const QString &o)
{
    if(orderListsBy != o)
    {
        orderListsBy = o;
        emit orderListsByChanged(orderListsBy);
    }
}

unsigned short SurfaceCommunicator::getPlayers() const
{
    return(players);
}

void SurfaceCommunicator::setPlayers(unsigned short p)
{
    if(players != p)
    {
        players = p;
        emit playersChanged(players);
    }
}

QString SurfaceCommunicator::getSortBy() const
{
    return(sortBy);
}

void SurfaceCommunicator::setSortBy(const QString &s)
{
    if(sortBy != s)
    {
        sortBy = s;
        emit sortByChanged(sortBy);
    }
}
