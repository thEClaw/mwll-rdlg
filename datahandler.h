/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef DATAHANDLER_H
#define DATAHANDLER_H

#include <QList>
#include <QObject>
#ifndef CONSOLE_ONLY
    #include <QQmlListProperty>
#endif

class DataHandler : public QObject //read in data necessary for the assets and criteria; write it, too (as a QSettings-object, no matter the input-format)
{
    Q_OBJECT
    Q_PROPERTY(bool cancelGeneration MEMBER cancelGeneration)
#ifndef CONSOLE_ONLY
    Q_PROPERTY(QQmlListProperty<AbstractCriterion> qmlCriteria READ qmlCriteria NOTIFY criteriaChanged)
#endif
    Q_CLASSINFO("DefaultProperty", "qmlCriteria")
public:
    DataHandler(class SurfaceCommunicator *s);

#ifndef CONSOLE_ONLY
    QQmlListProperty<class AbstractCriterion> qmlCriteria(); //make criteria accessible via Qml
#endif

    Q_INVOKABLE void generate();
    Q_INVOKABLE void reload();
    Q_INVOKABLE void saveSettings() const;

    bool ExtendAssets(const QString &file);
    bool LoadAssets();
    Q_INVOKABLE bool saveAssets() const;
public slots:
    void copyToClipboard(const QString &text);
    void saveLists(const QString &text);
signals:
    void allListsDone();
    void criteriaChanged();
    void listDone(const QString &list);
    void message(const QString &msg);
private:
    QMultiMap<double, QObject *> CheckAllCriteria(const QList<QObject *> &items, const QList<QObject *> &previouslyPicked, const QList<class AbstractCriterion*> &criteria) const;
    void SortAssets(QList<class Asset *> &items, const QString &value) const;

    bool cancelGeneration;

    QList<class Asset*> assets;
    QList<class AbstractCriterion*> criteria;//general criteria for filtering assets and helping order them by badness

#ifndef CONSOLE_ONLY
    //make criteria accessible via Qml
    static void appendCriterion(QQmlListProperty<AbstractCriterion> *list, AbstractCriterion *crt);
    static AbstractCriterion *atCriterion(QQmlListProperty<AbstractCriterion> *list, int index);
    static void clearCritera(QQmlListProperty<AbstractCriterion> *list);
    static int countCriteria(QQmlListProperty<AbstractCriterion> *list);
#endif

    class SurfaceCommunicator *comm;//not created by this class, so don't delete it!
};

#endif // DATAHANDLER_H
