/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef CONSOLE_ONLY
    #include <QApplication>
    #include <QClipboard>
#else
    #include <QCoreApplication>
#endif
#include <QDateTime>
#include <QElapsedTimer>
#include <QFile>
#include <QFileInfo>
#include <QSettings>

#include "criteria/abstractcriterion.h"
#include "asset.h"
#include "criteria/budget.h"
#include "datahandler.h"
#include "displayhandler.h"
#include "rng.h"
#include "surfacecommunicator.h"

#ifdef QT_DEBUG
    #define ASSET_FILE "../assets_xInVicTuSx.ini"
#else
    #define ASSET_FILE "assets.ini"
#endif
#define ASSET_EXTENSION_FILE "assets_ext.csv"
#define CRITERIA_FILE "criteria.ini"
#define SETTINGS_FILE "settings.ini"
#define SAVED_LISTS "stored_lists.txt"

DataHandler::DataHandler(SurfaceCommunicator *s) : cancelGeneration(false), comm(s)
{
    this->reload();
}






class ItemLoader : public QObject
{
    Q_OBJECT
};





class CriterionLoader : public QObject
{
    Q_OBJECT
public:
    // save to and load from file
    Q_INVOKABLE static QList<AbstractCriterion*> loadIni(const QString& file);
    Q_INVOKABLE static void saveIni(const QList<AbstractCriterion*>& criteria, const QString& file);
private:
    CriterionLoader(QObject* parent) {}
};

QList<AbstractCriterion*> CriterionLoader::loadIni(const QString& file)
{
    QList<AbstractCriterion*> criteria;
    if(!QFile::exists(file))
    {
        return(criteria);
    }

    QSettings settings(file, QSettings::IniFormat);
    QStringList children = settings.childGroups();
    for(auto child = children.constBegin(); child != children.constEnd(); ++child)
    {
        settings.beginGroup(*child);

        QString type = settings.value(QStringLiteral("type")).toString();
        AbstractCriterion *ac = AbstractCriterion::createCriterion(type, this);
        if(ac != nullptr)
        {
            QMap<QString, QVariant> data;
            QStringList keys = settings.allKeys();
            for(auto k = keys.constBegin(); k != keys.constEnd(); ++k)
            {
                data.insert(*k, settings.value(*k));
            }
            data.insert(QStringLiteral("identifier"), *child);

            if(!ac->load(data))
            {
                ac->deleteLater();
                continue;
            }
            criteria.append(ac);
        }

        settings.endGroup();
    }

    return(criteria);
}

void CriterionLoader::saveIni(const QList<AbstractCriterion*>& criteria, const QString& file)
{
    if (criteria.isEmpty() || file.isEmpty())
        return;
    QFile::remove(file);
    QSettings settings(file, QSettings::IniFormat);

    for(auto criterion = criteria.begin(); criterion != criteria.end(); ++criterion)
    {
        settings.beginGroup((*criterion)->objectName());

        QMap<QString, QVariant> data = (*criterion)->save();
        for(auto d = data.constBegin(); d != data.constEnd(); ++d)
        {
            settings.setValue(d.key(), d.value());
        }
        settings.endGroup();
    }
}




class CriterionManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool cancelChecks MEMBER cancelChecks)
public:
    CriterionManager(QObject *parent) : QObject(parent) {}

    //Q_INVOKABLE void appendCriterion(AbstractCriterion::CriterionType type);
    Q_INVOKABLE void appendCriterion(AbstractCriterion* criterion);
    Q_INVOKABLE void removeCriterion(int index);
    Q_INVOKABLE void removeCriterion(const QString& identifier);

    QMultiMap<double, QObject*> check(const QList<QObject*>& items, const QList<AbstractCriterion*>& criteria, const QList<QObject*>& previouslyPicked, int requiredPicks) const;
signals:
    void criteriaChanged();
private:
    bool cancelChecks;
    QList<AbstractCriterion*> criteria;
};

QMultiMap<double, QObject*> CriterionManager::check(const QList<QObject*>& items, const QList<AbstractCriterion*>& criteria, const QList<QObject*>& previouslyPicked, int requiredPicks) const
{
    QMultiMap<double, QObject*> rated;
    int i = 0;
    for(auto item = items.constBegin(); item != items.constEnd(); ++item, i++)
    {
        if(i % 20 == 0) // let the UI breathe (if there is one)
        {
            qApp->processEvents();
            if(cancelChecks)
                break;
        }

        double badness = 0;
        bool passed = false;
        for(auto criterion = criteria.constBegin(); criterion != criteria.constEnd(); ++criterion)
        {
            double bn = 0;
            if(!(passed = (*criterion)->check(*item, previouslyPicked, requiredPicks, &bn)))
                break;
            badness += bn;
        }

        if (passed)
        {
            badness /= criteria.count(); // normalize
            rated.insert(badness, *item);
        }
    }
    return(rated);
}

void CriterionManager::appendCriterion(AbstractCriterion *criterion)
{
    criteria.append(criterion);
    emit criteriaChanged();
}

void CriterionManager::removeCriterion(int index)
{
    if (index < 0 || index >= criteria.count())
        return;
    QObject* c = criteria.takeAt(index);
    delete c;
    emit criteriaChanged();
}

void CriterionManager::removeCriterion(const QString& identifier)
{
    bool deleted = false;
    for (int i = criteria.count() - 1; i >= 0; i --)
    {
        if (criteria[i]->objectName() == identifier)
        {
            QObject* c = criteria.takeAt(i);
            delete c;
            deleted = true;
        }
    }
    if (deleted)
        emit criteriaChanged();
}









void DataHandler::SortAssets(QList<Asset *> &items, const QString &value) const
{
    auto sorter = [&value](Asset *a, Asset *b)
    {
        if(value.compare(QLatin1String("price"), Qt::CaseInsensitive) == 0)
        {
            return(a->getPrice() < b->getPrice());
        }
        else if(value.compare(QLatin1String("mass"), Qt::CaseInsensitive) == 0)
        {
            return(a->getMass() < b->getMass());
        }
        else
        {
            QVariant av = a->getValue(value);//could contain a string, a bool (encoded as string - "true" or "false") or a number (double)
            QVariant bv = b->getValue(value);

            //check for "double"
            if(av.canConvert<double>() && bv.canConvert<double>())
            {
                bool ok = false;
                av.toDouble(&ok);
                if(ok)
                {
                    bv.toDouble(&ok);
                }
                if(ok)
                {
                    return(av.toDouble() < bv.toDouble());
                }
            }

            //check for "string"
            if(av.canConvert<QString>() && bv.canConvert<QString>())
            {
                return(av.toString() < bv.toString());
            }
            else //use QVariant-based comparison
            {
                return QVariant::compare(av, bv) != 0;
            }
        }
    };
    std::sort(items.begin(), items.end(), sorter);
}

void DataHandler::generate()
{
    QList<Asset *> picked;

    int players = comm->getPlayers();

    QElapsedTimer e;
    int maxAssetsFound;
    int failedCounter;
    QMultiMap<double, Asset *> filteredStatic = this->CheckAllCriteria(assets, picked, players, criteria);
    DisplayHandler display;

    //prepare Criteria (necessary only once)
    for(auto c = criteria.constBegin(); c != criteria.constEnd(); ++c)
    {
        (*c)->prepareGlobal(assets);
    }

    for(unsigned int list = 0; list < comm->getLists(); list ++)
    {
        e.start();

        maxAssetsFound = 0;
        failedCounter = 0;

redo:
        if(cancelGeneration)
        {
            //qWarning("Generation cancelled.");
            emit message(QStringLiteral("Generation cancelled."));
            goto end;
        }

        picked.clear();
        if(failedCounter > 10000)
        {
            //qWarning("No lists found after 10000 tries - please check your criteria/settings.");
            emit message(QStringLiteral("No lists found after 10000 tries - please check your criteria/settings."));
            emit allListsDone();
            return;
        }

        //prepare Criteria (necessary per list)
        for(auto c = criteria.constBegin(); c != criteria.constEnd(); ++c)
        {
            (*c)->preparePerList(filteredStatic.values());
        }

        if(failedCounter % 10 == 0) //might get choppy on single-threaded devices (but I don't know if these even exist anymore)
        {
            qApp->processEvents();/*-----slows down the program by quite a lot*/
        }

        if(failedCounter % 50 == 0 && failedCounter != 0)
        {
            QString text;
            text = QString("%i/%i lists finished, failed tries for current list: %4i, Highest amount of assets found during the last 50 tries: %2i/%2i")
                    .arg(display.count()).arg(comm->getLists()).arg(failedCounter).arg(maxAssetsFound).arg(players);
            //qWarning("%s", qPrintable(text));
            emit message(text);
            maxAssetsFound = 0;
        }

        //generate the correct amount of assets
        for(int toPick = 0; toPick < players; toPick ++)
        {
            QMultiMap<double, Asset *> filtered = this->CheckAllCriteria(filteredStatic.values(), picked, criteria);

            if(filtered.isEmpty())
            {
                break;
            }
            else if(filtered.count() < players - toPick/* && !comm->getDuplicates()*/)//not enough assets left to generate a complete list
            {
                //qDebug("Not enough assets left to generate a full list.");
                break;
            }

            //process other filters/settings
            {
                //check if budgets still allow for a solution
                //qDebug("%i", toPick);
                for(auto c = criteria.constBegin(); c != criteria.constEnd(); ++c)
                {
                    Criterion_TotalBudget *cb = qobject_cast<Criterion_TotalBudget *>(*c);
                    if(cb && cb->getEnabled())
                    {
                        double cost = 0;
                        for(auto a = picked.constBegin(); a != picked.constEnd(); ++a)
                        {
                            cost += cb->getCost(*a, nullptr);
                        }

                        const QString name = cb->getUsedAttribute();
                        double budget = cb->getBudget(players);
                        double deviation = budget * cb->getSoftness();

                        //QList<Asset *> budgetSorted(filtered.values());
                        //this->SortAssets(budgetSorted, name);

                        double min = std::numeric_limits<double>::max();
                        double max = std::numeric_limits<double>::min();
                        for(auto a = filtered.constBegin(); a != filtered.constEnd(); ++a)
                        {
                            min = qMin(min, (*a)->getValue(name).toDouble());
                            max = qMax(max, (*a)->getValue(name).toDouble());
                        }

                        if(min * (players - toPick) > budget * cb->getMaxPercentage() + deviation - cost) //not enough resources left to complete the list
                        {
                            failedCounter ++;
                            maxAssetsFound = qMax(picked.count(), maxAssetsFound);
                            if(!picked.isEmpty())
                            {
                                goto redo;
                            }
                            else
                            {
                                QString text;
                                text = QString("No droplist can be generated (budget \"%s\" too small).").arg(cb->objectName());
                                //qWarning("%s", qPrintable(text));
                                emit message(text);
                                emit allListsDone();
                                return;
                            }
                        }

                        if(max * (players - toPick) < budget * cb->getMinPercentage() - deviation - cost) //too many resources left
                        {
                            failedCounter ++;
                            maxAssetsFound = qMax(picked.count(), maxAssetsFound);
                            if(!picked.isEmpty())
                            {
                                goto redo;
                            }
                            else
                            {
                                QString text;
                                text = QString("No droplist can be generated (budget \"%s\" too high).").arg(cb->objectName());
                                //qWarning("%s", qPrintable(text));
                                emit message(text);
                                emit allListsDone();
                                return;
                            }
                        }
                    }
                }
            }
            //filtered now contains all "allowed" assets, ordered by badness

            if(filtered.isEmpty())
            {
                break;
            }
            else if(filtered.count() < players - toPick/* && !comm->getDuplicates()*/)//not enough assets left to generate a complete list
            {
                //qDebug("Not enough assets left to generate a full list.");
                break;
            }

            //qDebug("Minimum badness: %lf (%s), maximum badness: %lf (%s), %i assets in total", filtered.firstKey(), qPrintable(filtered.first()->getName()), filtered.lastKey(), qPrintable(filtered.last()->getName()), filtered.count());

            int index = pickIndex(filtered.keys(), comm->getGaussianWidth());//keys are ordered, including multiple occurences
#ifdef QT_DEBUG
//            qDebug("Pick: %i\n", toPick + 1);
//            for(auto i = filtered.constBegin(); i != filtered.constEnd(); ++i)
//            {
//                qDebug("%3.6lf - %s", i.key(), qPrintable(i.value()->getName()));
//            }
#endif
            if(index < 0)
            {
                //qDebug("The RNG cut out all the assets - retrying. (%i)", index);//shouldn't happen anymore
                break;
            }
            Asset *pickedAsset = filtered.values().at(index);

            //do post-process-things
            {
                //...
            }

            picked.append(pickedAsset);
        }

        //check Budget-Criteria; it is possible that the spent budget is too low (it will NEVER be too high at this point)
        bool redo = false;
#ifdef QT_DEBUG
        QString vetoReason = "";
#endif
        for(auto c = criteria.constBegin(); c != criteria.constEnd(); ++c)
        {
            Criterion_TotalBudget *cb = qobject_cast<Criterion_TotalBudget *>(*c);
            if(cb && cb->getEnabled())
            {
                double cost = 0;
                for(auto a = picked.constBegin(); a != picked.constEnd(); ++a)
                {
                    cost += cb->getCost(*a, nullptr);
                }

                double max = cb->getBudget(players);
                if(cost < cb->getMinPercentage() * max - cb->getSoftness() * max
                        || cost > cb->getMaxPercentage() * max + cb->getSoftness() * max) //didn't spend right amount of the budget, generate a new list
                {
                    redo = true;
#ifdef QT_DEBUG
                    QString reason = cb->getUsedAttribute() + QString(": %1/%2").arg(cost, 0, 'f', 0).arg(max, 0, 'f', 0);
                    vetoReason += vetoReason.count() > 0 ? "; " + reason : reason;
#else
                    break;
#endif
                }
            }
        }

        if(redo)
        {
#ifdef QT_DEBUG
            QStringList ps;
            for(auto i = picked.constBegin(); i != picked.constEnd(); ++i)
            {
                ps += (*i)->getName();
            }
            qWarning("Budget(s) unmet (%i/%i).\n%s", (int)picked.count(), players, qPrintable(vetoReason)); // NOTE: Allow the full long long instead of just the int.
            qWarning("%s", qPrintable(ps.join(", ") + "\n"));
#endif
            maxAssetsFound = qMax(picked.count(), maxAssetsFound);
            failedCounter ++;
            goto redo;
        }

        //check if enough assets were found
        maxAssetsFound = qMax(picked.count(), maxAssetsFound);
        if(picked.count() != players)
        {
            failedCounter ++;
            goto redo;
        }

        //generate the output
        this->SortAssets(picked, QStringLiteral("name"));//pre-sort so the sorting in the next line looks more intuitive
        this->SortAssets(picked, comm->getSortBy().toLower());

        //hand the finished list over to the display class
        display.addList(picked, e.nsecsElapsed() / 1000.0 / 1000.0 / 1000.0, failedCounter);

        QString text;
        text = QString("%i/%i lists finished.").arg(display.count()).arg(comm->getLists());
        emit message(text);

        if(!comm->getNoOutput() && comm->getOrderListsBy() == QLatin1String("No List Order"))//print a single list
        {
            emit listDone(DisplayHandler::writeTextOutput(picked, players, failedCounter, e.nsecsElapsed() / 1000.0 / 1000.0 / 1000.0));
        }
    }

end://send out whatever data whas created
    if(display.count())
    {
        if(comm->getCreateStatisticalOverview())
        {
            display.plotStatistics(false, assets, QStringLiteral("statistics_detailed.png"));
            display.plotStatistics(true, assets, QStringLiteral("statistics_total.png"));//show how many of each asset were handed out
        }
        if(!comm->getNoOutput() && comm->getCopyResultsToClipboard())
        {
            this->copyToClipboard(display.writeLists(comm->getOrderListsBy()));
        }
        if(!comm->getNoOutput() && comm->getOrderListsBy() != QLatin1String("No List Order"))//otherwise all lists have been printed before
        {
            emit listDone(display.writeLists(comm->getOrderListsBy()));
        }
    }
    emit allListsDone();
}

bool DataHandler::ExtendAssets(const QString &file)
{
    //Checks ASSET_EXTENSION_FILE for additional data that should be loaded into the assets.

    if(!QFile::exists(file))
    {
        return(false);
    }

    //format: "asset_name;category_name;data_without_semicolons"
    QFile f(file);
    f.open(QIODevice::ReadOnly);

    QStringList items;
    while(!f.atEnd())
    {
        items.append(QString::fromLocal8Bit(f.readLine()).remove(QLatin1Char('\n')).remove(QLatin1Char('\r')));
    }
    f.close();

    QMap<const Asset *, int> changeCounter;//for checking if all assets in the original ASSETS_FILE were treated in ASSET_EXTENSION_FILE
    for(auto a = assets.begin(); a != assets.end(); ++a)
    {
        changeCounter.insert(*a, 0);
    }

    for(auto i = items.constBegin(); i != items.constEnd(); ++i)
    {
        QStringList row = i->split(QLatin1Char(';'));
        if(row.count() < 2)
        {
            continue;
        }

        for(auto a = assets.begin(); a != assets.end(); ++a)
        {
            if((*a)->getName().compare(row.first()) == 0)
            {
                if(row.count() <= 2)
                {
                    (*a)->setValue(row.at(1), QLatin1String(""));
                }
                else if(row.count() > 2 && !row.at(2).contains(QLatin1Char(',')))
                {
                    (*a)->setValue(row.at(1), row.at(2));
                }
                else
                {
                    QString d = row.at(2);
                    d = d.replace(QLatin1String(", "), QLatin1String(",")).replace(QLatin1String(" ,"), QLatin1String(","));
                    (*a)->setValue(row.at(1), d.split(QLatin1Char(','), Qt::SkipEmptyParts));
                }
                changeCounter.insert(*a, changeCounter.value(*a) + 1);
            }
        }
    }

    //list unchanged assets as insurance
    QStringList bad;
    int smallest = INT_MAX;
    int biggest = 0;
    //determine least and most changes to different assets
    for(auto c = changeCounter.constBegin(); c != changeCounter.constEnd(); ++c)
    {
        smallest = qMin(c.value(), smallest);
        biggest = qMax(c.value(), biggest);
    }

    if(smallest == biggest)
    {
        if(smallest == 0)
        {
            QString text;
            text = QString("No asset has been extended.");
            qWarning("%s", qPrintable(text));
            emit message(text);
        }
        return(true);
    }

    for(auto c = changeCounter.constBegin(); c != changeCounter.constEnd(); ++c)
    {
        if(c.value() != biggest)
        {
            bad.append(c.key()->getName() + QLatin1String(" (") + QString::number(c.value() - biggest) + QLatin1String(")"));
        }
    }

    QString text;
    text = QString("Asset-extensions found and loaded, non-extended assets: %i assets\n%s").arg(bad.count()).arg(bad.join(", "));
    qWarning("%s", qPrintable(text));
    emit message(text);
    return(true);
}

bool DataHandler::LoadAssets()
{

    if(!QFile::exists(ASSET_FILE))
    {
        return(false);
    }

    QSettings s(ASSET_FILE, QSettings::IniFormat);

    QStringList as = s.childGroups();
    for(auto a = as.constBegin(); a != as.constEnd(); ++a)
    {
        s.beginGroup(*a);

        QMultiMap<QString, QVariant> d;
        QStringList keys = s.allKeys();
        for(auto k = keys.constBegin(); k != keys.constEnd(); ++k)
        {
            d.insert(*k, s.value(*k));
        }
        s.endGroup();

        assets.append(new Asset(*a, d, this));
    }

    return(true);
}

bool DataHandler::saveAssets() const
{
    QFile::remove(ASSET_FILE);

    QSettings s(ASSET_FILE, QSettings::IniFormat);

    for(auto a = assets.constBegin(); a != assets.constEnd(); ++a)
    {
        const QMultiMap<QString, QVariant> m = (*a)->getData();

        s.beginGroup((*a)->getName());
        for(auto d = m.constBegin(); d != m.constEnd(); ++d)
        {
            if(d.key() == QLatin1String("__fullname__"))
            {
                continue;
            }
            s.setValue(d.key(), d.value());
        }
        s.endGroup();
    }

    return(true);
}

void DataHandler::reload()
{
    qDeleteAll(assets);
    assets.clear();

    QList<AbstractCriterion *> cp(criteria);//workaround to not make QML crash
    criteria.clear();
    emit criteriaChanged();
    qDeleteAll(cp);

    this->LoadAssets();
    this->ExtendAssets(ASSET_EXTENSION_FILE);

    this->LoadCriteria();
    emit criteriaChanged();

    comm->Load(SETTINGS_FILE);
}

void DataHandler::saveSettings() const
{
    comm->Save(SETTINGS_FILE);
}

void DataHandler::copyToClipboard(const QString &text)
{
#ifndef CONSOLE_ONLY
    QApplication::clipboard()->setText(text);
#else
    Q_UNUSED(text)
#endif
}

void DataHandler::saveLists(const QString &text)
{
    QFileInfo info(SAVED_LISTS);

    QString out;
    if(info.size())
    {
        out = QStringLiteral("\n\n____________________________________________________________________________________________________________________________________________\n");
    }

    QFile f(SAVED_LISTS);
    f.open(QIODevice::Append);

    QString allCrit;
    for(auto c = criteria.constBegin(); c != criteria.constEnd(); ++c)
    {
        QString crit = (*c)->print();
        if(!crit.isEmpty())
        {
            if(!allCrit.isEmpty())
            {
                allCrit += QLatin1String("\n");
            }
            allCrit += crit;
        }
    }
    if(!allCrit.isEmpty())
    {
        allCrit += QLatin1String("\n\n");
    }

    f.write(out.toUtf8());

    out = QDateTime::currentDateTime().toString(QStringLiteral("dd/MM/yyyy hh:mm:ss t")) + QLatin1String(":\n");
    f.write(out.toUtf8());
    f.write(allCrit.toUtf8());
    f.write(text.toUtf8());

    f.close();
}

//_________________Qml-access to "criteria"________________

#ifndef CONSOLE_ONLY
QQmlListProperty<AbstractCriterion> DataHandler::qmlCriteria()
{
    return(QQmlListProperty<AbstractCriterion>(this, nullptr, &DataHandler::appendCriterion, &DataHandler::countCriteria,
                                               &DataHandler::atCriterion, &DataHandler::clearCritera));//doesn't work
}

void DataHandler::appendCriterion(QQmlListProperty<AbstractCriterion> *list, AbstractCriterion *crt)
{
    DataHandler *t = qobject_cast<DataHandler *>(list->object);
    if(t && crt)
    {
        t->criteria.append(crt);
        emit t->criteriaChanged();
    }
}

AbstractCriterion *DataHandler::atCriterion(QQmlListProperty<AbstractCriterion> *list, int index)
{
    DataHandler *t = qobject_cast<DataHandler *>(list->object);
    if(t && t->criteria.count() > index)
    {
        return(t->criteria.at(index));
    }
    return(0);
}

void DataHandler::clearCritera(QQmlListProperty<AbstractCriterion> *list)
{
    DataHandler *t = qobject_cast<DataHandler *>(list->object);
    if(t)
    {
        qDeleteAll(t->criteria);
        t->criteria.clear();
        emit t->criteriaChanged();
    }
}

int DataHandler::countCriteria(QQmlListProperty<AbstractCriterion> *list)
{
    DataHandler *t = qobject_cast<DataHandler *>(list->object);
    if(t)
    {
        return(t->criteria.count());
    }
    return(0);
}
#endif
