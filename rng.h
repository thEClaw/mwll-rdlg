/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef RNG
#define RNG

#include <QList>

int pickIndex(const QList<double> &l, double HWHM = 0.4); //could return -1 in case no result is found
//HWHM = Half width at half maximum, rough estimate of what values are likely to be still included (don't forget that the badness is the sum of all criterias badnesses, so don't make HWHM too small)

#endif // RNG
