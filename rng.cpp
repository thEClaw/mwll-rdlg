/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#include <QDateTime>

#include "rng/randomc/randomc.h" //contains Mersenne-Twister et al.
#include "rng/stocc/stocc.h" //can create non-uniform distributions out of uniform ones

#define STOC_BASE CRandomMersenne

StochasticLib1 sto(int(QDateTime::currentMSecsSinceEpoch() % 1000000));//seed

int pickIndex(const QList<double> &l, double HWHM)
{
//    double Normal(double m, double s);
//    Normal distribution with mean m and standard deviation s

    //Step 0: calculate sigma from HWHM
    double sigma = 2 * HWHM / 2.35482; //2 * sqrt(2*ln(2))

    //Step 1: randomly pick a badness-limit
    double badnessLimit = qMax(qAbs(sto.Normal(0, sigma)), l.first());//make sure at least one element is within the valid area
    //qDebug("%2.3lf %2.3lf %2.3lf", HWHM, sigma, badnessLimit);

    //Step 2: find the maximum index of the list of badnesses that can be used
    int index;
    for(index = 0; index < l.count(); index++)
    {
        if(l.at(index) > badnessLimit) //allowed values have to be <= in case of badnessLimit being 0.0 - or no values might be allowed
        {
            break;
        }
    }
    index --;

    if(!(index < 0))
    {
        index = sto.IRandom(0, index);
    }

    //Step 3: pick an item from the "allowed"-list (via an even distribution this time)
    return(index);//both boundaries are possible values
}
