/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

StatusBar {
    Timer {
        id: statusCleanup
        interval: 10000 //not working properly until I put the generate()-method into another thread (since the timer is dependent on FPS)
        running: false
        repeat: false
        onTriggered: {
            statusLabel.text = ""
        }
    }

    RowLayout {
        Label {
            id: statusLabel
            onTextChanged: {
                statusCleanup.restart()
            }
            text: "This program is distributed under LGPL-3.0 and has been created using Qt (www.qt.io). tcl, 2015"
        }
    }

    Connections {
            target: handler
            onMessage: {
                statusLabel.text = msg
            }
    }
}

