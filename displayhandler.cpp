/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef CONSOLE_ONLY
    #include <QPainter>
#endif
#include <QVariant>

#include "asset.h"
#include "displayhandler.h"

QStringList ResultList::orderBy = QStringList();//prepare static variable

DisplayHandler::~DisplayHandler()
{
    this->clearLists();
}

void DisplayHandler::addList(QList<Asset *> picked, double seconds, int failedTries)
{
    double tonnage = 0;
    double BV = 0;
    double cost = 0;

    QList<QPointer<Asset> > p;
    for(auto i = picked.begin(); i != picked.end(); ++i)
    {
        tonnage += (*i)->getMass();
        BV += (*i)->getValue(QStringLiteral("battlevalue")).toDouble();
        cost += (*i)->getPrice();

        p.append(QPointer<Asset>(*i));
    }
    lists.append(ResultList(p, tonnage, BV, cost, seconds, failedTries));
}

void DisplayHandler::clearLists()
{
    lists.clear();
}

int DisplayHandler::count() const
{
    return(lists.count());
}

QString DisplayHandler::writeLists(const QString &order) const
{
    QString totalOut;

    QList<class ResultList> cpLists(lists);
    ResultList::setOrder(order);//order things appropriately
    std::sort(cpLists.begin(), cpLists.end());

    for(auto l = cpLists.constBegin(); l != cpLists.constEnd(); ++l)
    {
        QString memcp;
        QString temp;

        temp = QString("Picked the following assets (%i in total) after %i failed tries (%3.3lfs):").arg(l->getResults().count()).arg(l->getFailedTries()).arg(l->getSeconds());
        memcp.append(temp + QLatin1Char('\n'));

        for(auto c = l->getResults().constBegin(); c != l->getResults().constEnd(); ++c)
        {
            if(!(*c))//make sure the pointer is non-zero
            {
                continue;
            }

            QString equipment = (*c)->getValue(QStringLiteral("equipment")).toStringList().join(QStringLiteral(", "));
            if(equipment.count() == 0)
            {
                equipment = "\u2014";
            }

            //temp.sprintf("       %3i t, %6i CBills, %1i    BV, %s - %s - %s", int((*c)->getMass()), int((*c)->getPrice()), (*c)->getValue(QStringLiteral("battlevalue")).toInt(),
            temp = QString("       %3i t, %1i    BV, %6i CBills, %s - %s - %s")
                    .arg((*c)->getMass())
                    .arg((*c)->getValue(QStringLiteral("battlevalue")).toInt())
                    .arg((*c)->getPrice())
                    .arg((*c)->getValue(QStringLiteral("name")).toString() + QLatin1String(" (") + (*c)->getValue(QStringLiteral("faction")).toStringList().join(QStringLiteral(", "))  + QLatin1String(")"))
                    .arg(equipment)
                    .arg((*c)->getValue(QStringLiteral("weapons_short")).toStringList().join(QStringLiteral(", ")));

            memcp.append(temp + QLatin1Char('\n'));
        }
        //temp.sprintf("Total: %3i t, %6i CBills, %1.2lf BV", int(l->getTonnage()), int(l->getCost()), l->getBv() / l->getResults().count());
        temp = QString("Total: %3i t, %1.2lf BV, %6i CBills").arg(l->getTonnage()).arg(l->getBv() / l->getResults().count()).arg(int(l->getCost()));
        memcp.append(temp);

        totalOut.append(totalOut.isEmpty() ? memcp : QLatin1String("\n\n") + memcp);
    }
    return(totalOut);
}

QString DisplayHandler::writeTextOutput(const QList<Asset *> &picked, int players, int failedTries, double seconds)
{
    double tonnage = 0;
    double cost = 0;
    double BV = 0;

    QString memcp;
    QString temp;

    temp = QString("Picked the following assets (%i/%i in total) after %i failed tries (%3.3lfs):").arg(picked.count()).arg(players).arg(failedTries).arg(seconds);
    memcp.append(temp + QLatin1Char('\n'));
    for(auto c = picked.constBegin(); c != picked.constEnd(); ++c)
    {
        tonnage += (*c)->getMass();
        cost += (*c)->getPrice();
        BV += (*c)->getValue(QStringLiteral("battlevalue")).toDouble();

        QString equipment = (*c)->getValue(QStringLiteral("equipment")).toStringList().join(QStringLiteral(", "));
        if(equipment.count() == 0)
        {
            equipment = "\u2014";
        }

        //temp.sprintf("       %3i t, %6i CBills, %1i    BV, %s - %s - %s", int((*c)->getMass()), int((*c)->getPrice()), (*c)->getValue(QStringLiteral("battlevalue")).toInt(),
        temp = QString("       %3i t, %1i    BV, %6i CBills, %s - %s - %s")
                .arg((*c)->getMass())
                .arg((*c)->getValue(QStringLiteral("battlevalue")).toInt())
                .arg((*c)->getPrice())
                .arg((*c)->getValue(QStringLiteral("name")).toString() + QLatin1String(" (") + (*c)->getValue(QStringLiteral("faction")).toStringList().join(QStringLiteral(", "))  + QLatin1String(")"))
                .arg(equipment)
                .arg((*c)->getValue(QStringLiteral("weapons_short")).toStringList().join(QStringLiteral(", ")));

        memcp.append(temp + QLatin1Char('\n'));
    }
    //temp.sprintf("Total: %3i t, %6i CBills, %1.2lf BV", int(tonnage), int(cost), BV / players);
    temp = QString("Total: %3i t, %1.2lf BV, %6i CBills").arg(tonnage).arg(BV / players).arg(cost);
    memcp.append(temp);

    return(memcp);
}

QMap<QString, QMap<QString, int> > DisplayHandler::gatherStatistics(const QList<ResultList> &l) const
{
    QMap<QString, QMap<QString, int> > results;//QMap<QString parent_vehicle, QMap<QString name, int count> >
    for(auto i = l.constBegin(); i != l.constEnd(); ++i)
    {
        for(auto c = i->getResults().constBegin(); c != i->getResults().constEnd(); ++c) //c = QPointer<class Asset> out of a single result-list
        {
            if(!(*c))//make sure the pointer is non-zero
            {
                continue;
            }

            //order output by something other than names
            QString lookup = (*c)->getValue(QStringLiteral("parent_vehicle")).toString();
            //QString lookup = QString::number((*c)->getMass()).rightJustified(3, ' ') + QLatin1String("t ") + (*c)->getValue(QStringLiteral("parent_vehicle")).toString();
            //QString lookup = QString::number((*c)->getPrice()).rightJustified(6, ' ') + QLatin1String("CB ") + (*c)->getValue(QStringLiteral("parent_vehicle")).toString();

            QMap<QString, int> map = results.value(lookup);
            map.insert((*c)->getValue(QStringLiteral("name")).toString(), map.value((*c)->getValue(QStringLiteral("name")).toString(), 0) + 1);
            results.insert(lookup, map);
        }
    }
    return(results);
}

bool DisplayHandler::plotStatistics(bool totOnly, const QList<Asset *> &other, const QString &filename) const
{
#ifndef CONSOLE_ONLY
    QMap<QString, QMap<QString, int> > items = this->gatherStatistics(lists);
    if(items.isEmpty())
    {
        return(false);
    }

    //add in assets that might not occur in "items"
    for(auto i = other.constBegin(); i != other.constEnd(); ++i)
    {
        QString lookup = (*i)->getValue(QStringLiteral("parent_vehicle")).toString();
        //QString lookup = QString::number((*i)->getMass()).rightJustified(3, ' ') + QLatin1String("t ") + (*i)->getValue(QStringLiteral("parent_vehicle")).toString();
        //QString lookup = QString::number((*i)->getPrice()).rightJustified(6, ' ') + QLatin1String("CB ") + (*i)->getValue(QStringLiteral("parent_vehicle")).toString();

        QMap<QString, int> map = items.value(lookup);
        if(!map.contains((*i)->getValue(QStringLiteral("name")).toString()))
            map.insert((*i)->getValue(QStringLiteral("name")).toString(), 0);
        items.insert(lookup, map);
    }

    //determine total width of the graph
    QSize barOne(12, 8);//width and height for a bar with a single entry
    int gridAxisOverlap = 5;//pixels that the y-grid will overlap with the y-axis
    int gridGap = 5;//spacing between grid-markers (e.g. 5, 10, 15... or 25, 50, 75...)
    int axisYTextOffsetX = 2;//where should the y-axis description begin?
    int headingUpperYBoundary = 5;//space between heading and the top of the image
    int headingMarginY = 30;//room at the top for the heading
    int axisXTextOffsetY = 12;//gap between x-axis and text on the x-axis
    int marginX = 40;//includes y-axis descriptions *and* the space occupied by the last items on the x-axis (since they are rotated by 45 degrees, they need more space than the actual diagram)
    int marginY = 20;
    int marginYFont = 90;//margin below the diagram for descriptions of the assets
    float barGap = 0.5;//multiplied with barOne.width() whenever needed
    float bigGap = 2.0;//gap between different parent_vehicles; multiplied with barOne.width() whenever needed

    //determine total size
    int dataAreaX = 0;//total width of the data area
    int dataAreaY = 0;//total height of the data area
    for(auto i = items.constBegin(); i != items.constEnd(); ++i)
    {
        //width
        if(totOnly)
        {
            dataAreaX += barOne.width() * (1 + barGap);
        }
        else
        {
            dataAreaX += i.value().count() * barOne.width() * (1 + barGap);
            dataAreaX += barOne.width() * (1 + bigGap);
        }

        //height
        QList<int> values = i.value().values();
        std::sort(values.begin(), values.end());
        dataAreaY = qMax(dataAreaY, values.last() * barOne.height());
    }

    if(dataAreaY > 20000)
    {
        barOne.setHeight(barOne.height() / 8); dataAreaY /= 8; gridGap = 50;
    }
    else if(dataAreaY > 10000)
    {
        barOne.setHeight(barOne.height() / 4); dataAreaY /= 4; gridGap = 25;
    }
    else if(dataAreaY > 5000)
    {
        barOne.setHeight(barOne.height() / 2); dataAreaY /= 2; gridGap = 10;
    }

    //set the diagrams visual (0,0)
    QPoint offset(marginX, dataAreaY + marginY + headingMarginY);

    //prepare painter
    QPainter p;
    QPixmap out(dataAreaX + marginX * 2, dataAreaY + marginY * 2 + headingMarginY + marginYFont);
    out.fill();

    p.begin(&out);
    p.setPen(Qt::SolidLine);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setBrush(QBrush(Qt::blue));

    //draw y-grid
    p.drawLine(offset, QPoint(out.width() - marginX, offset.y()));//x axis
    p.drawLine(offset, QPoint(offset.x(), marginY + headingMarginY));//y axis
    QVector<QPoint> lines;
    for(int i = 1; offset.y() - i * barOne.height() * gridGap > marginY + headingMarginY; i ++)//every "gridGap" units there will be a grid-line
    {
        lines.append(QPoint(offset.x() - gridAxisOverlap, offset.y() - i * barOne.height() * gridGap));
        lines.append(QPoint(i % 2 ? offset.x() : out.width() - marginX, offset.y() - i * barOne.height() * gridGap));//only every second line forms a grid, the others are only very tiny markers on the y-axis
        if(i % 2 == 0 || (i == 1 && offset.y() - 2 * barOne.height() * gridGap <= marginY + headingMarginY))//draw number every two markers (unless there is only one marker)
        {
            int h = QFontMetrics(p.font()).height();
            p.drawText(QRect(0, offset.y() - i * barOne.height() * gridGap - int(h / 2.0 + 0.5), offset.x() - gridAxisOverlap - axisYTextOffsetX, h),
                       Qt::AlignTop | Qt::AlignRight, QString::number(i * gridGap));
        }
    }
    p.drawLines(lines);

    //draw heading
    p.save();
    QFont f = p.font();
    f.setPointSize(f.pointSize() * 2);
    f.setItalic(true);
    p.setFont(f);
    p.drawText(QRect(0, headingUpperYBoundary, out.width(), headingMarginY), Qt::AlignHCenter | Qt::AlignTop,
               QString(QLatin1String("%1 lists * %2 players = %3 entries")).arg(lists.count()).arg(lists.count() ? lists.first().getResults().count() : 0).arg(lists.count() * (lists.count() ? lists.first().getResults().count() : 0)));
    p.restore();

    for(auto i = items.constBegin(); i != items.constEnd(); ++i)
    {
        //draw variant-bars
        int tot = 0;
        for(auto e = i.value().constBegin(); e != i.value().constEnd(); ++e)
        {
            tot += e.value();
            if(!totOnly)
            {
                p.fillRect(offset.x(), offset.y(), barOne.width(), -barOne.height() * e.value(), Qt::blue);//at first offset still refers to the diagrams visual (0,0)

                p.save();
                p.translate(offset + QPoint(0, axisXTextOffsetY));
                p.rotate(45);
                p.setBrush(QBrush(Qt::black));
                p.drawText(0, 0, e.key());
                p.restore();

                offset += QPoint(barOne.width() * (1 + barGap), 0);
            }
        }

        //draw "summary bar" (i.e. added up variants)
        p.fillRect(offset.x(), offset.y(), barOne.width(), -barOne.height() * tot / i.value().count(), Qt::red);

        p.save();
        p.translate(offset + QPoint(0, axisXTextOffsetY));
        p.rotate(45);
        p.setBrush(QBrush(Qt::black));
        p.drawText(0, 0, totOnly ? i.key() : QStringLiteral("Total (Averaged)"));
        p.restore();

        offset += QPoint(barOne.width() * (1 + (totOnly ? barGap : bigGap)), 0);
    }
    p.end();

    out.save(filename);
#else
    Q_UNUSED(totOnly)
    Q_UNUSED(other)
    Q_UNUSED(filename)
#endif
    return(true);
}
