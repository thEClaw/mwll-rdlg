set xlabel "Cost"
set ylabel "Softness"
set zlabel "Badness"

c = 50000
b = 20000

# ranges for the budget-badness
set xrange [-c:c]
set yrange [0:1]
set zrange [0:10]

f1(x,y) = abs(x-b)/(y*b)
f2(x,y) = f1(x,y)**2
f3(x,y) = f1(x,y)**y

set samples 1000
splot f1(x,y), f2(x,y), f3(x,y)
