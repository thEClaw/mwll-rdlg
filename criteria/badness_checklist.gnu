set xlabel "Violations"
set ylabel "Softness"
set zlabel "Badness"

n = 5
n = 100

# ranges for the checklist-badness
set xrange [0:n]
set yrange [0:1]
set zrange [0:1]

f1(x,y) = (1.0-y)*(x/n)
f2(x,y) = (x/n)**y
f3(x,y) = f1(x,y)**y

splot f1(x,y), f2(x,y), f3(x,y)
