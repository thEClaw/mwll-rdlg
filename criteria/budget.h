/*
 * Copyright 2020 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef CBUDGET_H
#define CBUDGET_H

#include "abstractcriterion.h"

class Criterion_TotalBudget : public AbstractCriterionBudget
{
    /*
     * value-of-a-criterion-for-a-specific-item = v (found in the class for an item)
     * criterion-hardness * v = x
     * v-x < budget < v+x - decide whether item passes the criterion or not (note a "badness"-value by saving (v-criterion-maximum-value)/criterion-maximum-value;
     * in the end add up all the badnesses for every item and then use that for the sorting-step)
    */
    Q_OBJECT
    Q_PROPERTY(bool autoMultiply READ getAutoMultiply WRITE setAutoMultiply NOTIFY autoMultiplyChanged)
    Q_PROPERTY(double minPercentage READ getMinPercentage WRITE setMinPercentage NOTIFY minPercentageChanged)
    Q_PROPERTY(double maxPercentage READ getMaxPercentage WRITE setMaxPercentage NOTIFY maxPercentageChanged)
public:
    Criterion_TotalBudget(QObject* parent) : AbstractCriterionBudget(parent), autoMultiply(true), minPercentage(0.0), maxPercentage(1.0), _maxItemCost(1.0) {}

    virtual bool check(const QObject* item, const QList<QObject*>& previouslyPicked, int requiredPicks, double* badness) override;
    //badness is normalized to 1 (element [0,1]) and goes quadratically

    virtual void prepareGlobal(const QList<QObject*>& items) override; //finds the price of the most expensive item (depending on whatever budget this class represents)

    virtual bool load(const QMap<QString, QVariant>& information) override;
    virtual QMap<QString, QVariant> save() const override;

    virtual QString toString(bool includeDisabled = false) const override;

    double getBudget(int elements) const;

    inline bool getAutoMultiply() const { return autoMultiply; }
    inline double getMinPercentage() const { return minPercentage; }
    inline double getMaxPercentage() const { return maxPercentage; }
public slots:
    void setAutoMultiply(bool m);
    void setMinPercentage(double p);
    void setMaxPercentage(double p);
signals:
    void autoMultiplyChanged(bool);
    void minPercentageChanged(double);
    void maxPercentageChanged(double);
private:
    bool autoMultiply; // whether the provided budget is an "average per item" (autoMultiply = true) or a "total budget" (autoMultiply = false)
    double minPercentage;
    double maxPercentage;
    double _maxItemCost;
};

/*!
 * \brief The Criterion_SingleBudget class allows to check the cost of a single item against the budget for a single item.
 * If isLimit is true, the criterion will agree to anything below the budget. With inverse also true, anything above the budget is fine.
 * If isLimit is false, the criterion will prefer items that are closer to the budget limits over cheaper or more expensive ones.
 */
class Criterion_SingleBudget : public AbstractCriterionBudget
{
    Q_OBJECT
    Q_PROPERTY(bool inverse READ getInverse WRITE setInverse NOTIFY inverseChanged)
    Q_PROPERTY(bool isLimit READ getIsLimit WRITE setIsLimit NOTIFY isLimitChanged)
public:
    Criterion_SingleBudget(QObject* parent) : AbstractCriterionBudget(parent), inverse(false), isLimit(false) {}

    virtual bool check(const QObject* item, const QList<QObject*>& previouslyPicked, int requiredPicks, double* badness) override;
    //assigns a badness depending on how far an item strays from the set "single item budget"; might lead to cancellation of a whole list if the criterion can't be met anymore
    //badness is normalized to 1 (element [0,1]) and goes quadratically

    virtual bool load(const QMap<QString, QVariant>& information) override;
    virtual QMap<QString, QVariant> save() const override;

    virtual QString toString(bool includeDisabled = false) const override;

    inline bool getInverse() const { return inverse; }
    inline bool getIsLimit() const { return isLimit; }
public slots:
    void setInverse(bool inv);
    void setIsLimit(bool l);
signals:
    void inverseChanged(bool);
    void isLimitChanged(bool);
private:
    bool inverse; //no real meaning of !isLimit && inverse...so inverse is ignored in that case
    bool isLimit; // we just need to stay below the budget (inverse = false), nothing else is important
};

#endif // CBUDGET_H
