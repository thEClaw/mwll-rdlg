/*
 * Copyright 2020 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef CHECKLIST
#define CHECKLIST

#include <QStringList>

#include "abstractcriterion.h"

/*!
 * \brief The Criterion_Checklist class should only be executed once on the list of initial applicants to filter it down.
 * If inverse is true, none of the given values are allowed to be present. If inverse is false, at least one of the given values must be present.
 * This checks for an OR-combination of the elements in targetValues (AND if inverse is true). An AND can be simulated by having multiple of these criteria.
 */
class Criterion_Checklist : public AbstractCriterionChecklist
{
    Q_OBJECT
    Q_PROPERTY(bool inverse READ getInverse WRITE setInverse NOTIFY inverseChanged)
public:
    Criterion_Checklist(QObject* parent) : AbstractCriterionChecklist(parent), inverse(false) {}

    virtual bool check(const QObject* item, const QList<QObject*>& previouslyPicked, int requiredPicks, double* badness) override;

    virtual bool load(const QMap<QString, QVariant>& information) override;
    virtual QMap<QString, QVariant> save() const override;

    virtual QString toString(bool includeDisabled = false) const override;

    inline bool getInverse() const { return inverse; }
public slots:
    void setInverse(bool i);
signals:
    void inverseChanged(bool);
private:
    bool inverse; //if true, none of the items in targetValues are allowed to be contained by an item
};

/*!
 * \brief The Criterion_ChecklistMinMax class is used to reduce a list, depending on whether the min- or max-amounts are reached or not (in the latter case usually the criterion can remain "passive").
 * This checks for an OR-combination of the elements in getValues(). An AND can be simulated by having multiple of these criteria.
 */
class Criterion_ChecklistMinMax : public AbstractCriterionChecklist
{
    Q_OBJECT
    Q_PROPERTY(int amount READ getAmount WRITE setAmount NOTIFY amountChanged)
    Q_PROPERTY(QString typeOfCheck READ getTypeOfCheck WRITE setTypeOfCheck NOTIFY typeOfCheckChanged)
public:
    Criterion_ChecklistMinMax(QObject *parent) : AbstractCriterionChecklist(parent), amount(-1), typeOfCheck(QStringLiteral(">")), _pickedLengthBuffer(-1), _amountBuffer(-1) {}

    virtual bool check(const QObject* item, const QList<QObject*>& previouslyPicked, int requiredPicks, double* badness) override;

    virtual bool load(const QMap<QString, QVariant>& information) override;
    virtual QMap<QString, QVariant> save() const override;

    virtual QString toString(bool includeDisabled = false) const override;

    inline int getAmount() const { return amount; }
    inline QString getTypeOfCheck() const { return typeOfCheck; }
public slots:
    void setAmount(int a);
    void setTypeOfCheck(QString c);
signals:
    void amountChanged(int);
    void typeOfCheckChanged(QString);
private:
    bool checkSingle(const QString& need, const QObject* item) const;

    int amount;
    QString typeOfCheck; //"<", ">", "="/"==" (cases like "<=" don't need to be handled since you can easily "emulate" them by modifying "amount")
    int _pickedLengthBuffer; //stores the last length of the "previouslyPicked"-list handed to check() to speed up calculations
    int _amountBuffer;
};

#endif // CHECKLIST
