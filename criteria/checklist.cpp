/*
 * Copyright 2020 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#include <QtMath>
#include <QMetaEnum>
#include <QRegularExpression>

#include "checklist.h"

bool Criterion_Checklist::check(const QObject* item, const QList<QObject*>& previouslyPicked, int requiredPicks, double* badness)
{
    Q_UNUSED(previouslyPicked)
    Q_UNUSED(requiredPicks)
    if(!this->getEnabled() || !previouslyPicked.isEmpty()) // TODO: the latter one could cause problems if I ever forget about it, but: it should get rid of a little bit of overhead since "item" normally stems from a list that was already filtered using this Criterion
    {
        return(true);
    }

    bool result = true;
    double bn = MAXIMUM_BADNESS;
    int violations = 0;

    QStringList haves = AbstractCriterionChecklist::getItemValues(item, &result);
    const QStringList& needs = this->getTargetValues();
    if(needs.isEmpty())
        return(true);
    if(result)
    {
        for(auto need = needs.constBegin(); need != needs.constEnd(); ++need)
        {
            QRegularExpression r = QRegularExpression(this->getAllowPartialMatch() ? *need : QStringLiteral("^") + *need + "$", QRegularExpression::CaseInsensitiveOption | QRegularExpression::DontCaptureOption);
            if (inverse)
                violations += haves.filter(r).isEmpty() ? 0 : 1;
            else
                violations += haves.filter(r).isEmpty() ? 1 : 0;
        }

        result = (inverse ? violations == 0 : violations < needs.count());
        if(violations == 0)
        {
            bn = 0;
        }
        else
        {
            // the base rises linearly with softness and violations (see badness_checklist.gnu for a plot of this)
            bn = qPow((1.0 - this->getSoftness()) * (violations / needs.count()), this->getSoftness());
        }
    }

    if(badness)
    {
        *badness = bn;
    }
    return(result);
}

bool Criterion_Checklist::load(const QMap<QString, QVariant>& information)
{
    if(AbstractCriterionChecklist::load(information))
    {
        this->blockSignals(true);
        this->setInverse(information.value(QStringLiteral(u"inverse"), QStringLiteral(u"false")).toString() == "true");
        this->blockSignals(false);

        emit inverseChanged(inverse);
        return(true);
    }
    return(false);
}

QMap<QString, QVariant> Criterion_Checklist::save() const
{
    QMap<QString, QVariant> information = AbstractCriterionChecklist::save();
    QMetaEnum e = QMetaEnum::fromType<AbstractCriterion::CriterionType>();
    information.insert(QStringLiteral(u"type"), e.valueToKey(AbstractCriterion::CriterionType::Checklist));
    information.insert(QStringLiteral(u"inverse"), this->getInverse() ? "true" : "false");
    return(information);
}

void Criterion_Checklist::setInverse(bool i)
{
    if(inverse != i)
    {
        inverse = i;
        emit inverseChanged(inverse);
    }
}

QString Criterion_Checklist::toString(bool includeDisabled) const
{
    QString out;
    if(this->getEnabled() || includeDisabled)
    {
        out = QString("Criterion (Type): \"%1\" - Acting on property: \"%2\" - Softness: %3 - \"%4(%5)\"")
                .arg(this->objectName(), 35)
                .arg(this->getUsedAttribute(), 15)
                .arg(this->getSoftness(), 3, 'f', 2)
                .arg(this->getInverse() ? QStringLiteral(u"!") : QStringLiteral(u""))
                .arg(this->getTargetValues().join(QChar(',')));
    }
    return(out);
}

//_________________________________________________________

bool Criterion_ChecklistMinMax::check(const QObject* item, const QList<QObject*>& previouslyPicked, int requiredPicks, double* badness)
{
    if(!this->getEnabled())
    {
        return(true);
    }

    bool result = true;
    double bn = MAXIMUM_BADNESS;

    /*QStringList haves = */AbstractCriterionChecklist::getItemValues(item, &result);
    const QStringList& needs = this->getTargetValues();
    if(needs.isEmpty())
        return(true);
    if(result)
    {
        // initialize amountToGo to the actual amount of items we still have to find
        int amountToGo = qMin(amount, requiredPicks /*- (typeOfCheck == ">" ? 1 : 0)*/); //values are "OR"ed, hence amount is global
        // NOTE: I commented out the subtraction because I just couldn't figure out why it was ever there. We do correct for '<' and '>' a little further down anyway.

        // find out how many of the previouslyPicked items already fulfill our checks
        if(_pickedLengthBuffer != previouslyPicked.count())
        {
            _pickedLengthBuffer = previouslyPicked.count();
            for(auto pi = previouslyPicked.constBegin(); pi != previouslyPicked.constEnd(); ++pi) //collect information of previously picked items
            {
                for(auto need = needs.constBegin(); need != needs.constEnd(); ++need)
                {
                    if(this->checkSingle(*need, *pi))
                    {
                        amountToGo --;
                        break;
                    }
                }
            }
            _amountBuffer = amountToGo;
        }
        else
        {
            amountToGo = _amountBuffer;
        }

        if(typeOfCheck == "<") //make this "at most"...
        {
            amountToGo --;
        }
        else if(typeOfCheck == ">") //...and this "at least"
        {
            amountToGo ++;
        }

        // shortcut the case where there is definitely still enough room for us - we won't have to check anything then
        if (amountToGo <= 0 || requiredPicks - previouslyPicked.count() <= amountToGo)
        {
            for(auto need = needs.constBegin(); need != needs.constEnd(); ++need) //check for each value given
            {
                if(typeOfCheck == "<") //"at most amountToGo in requiredPicks"
                {
                    if(amountToGo <= 0) // we already have enough, so we have to check ourselves
                    {
                        result = !this->checkSingle(*need, item);
                        if(!result)
                        {
                            // the base rises linearly with softness and violations (see badness_checklist.gnu for a plot of this)
                            bn = qPow((1.0 - this->getSoftness()) * (-amountToGo + 1) / (requiredPicks - amount), this->getSoftness()); // amountToGo <=0 implies amount < requiredPicks
                            break;
                        }
                        else
                            bn = 0;
                    }
                    else // there is room left, so we don't care
                    {
                        bn = 0;
                        break;
                    }
                }
                else if(typeOfCheck == "=") //"exactly"
                {
                    if(amountToGo <= 0) // we already have enough, so we have to check ourselves
                    {
                        result = !this->checkSingle(*need, item);
                        if(!result)
                        {
                            bn = qPow((1.0 - this->getSoftness()) * (-amountToGo + 1) / (requiredPicks - amount), this->getSoftness());
                            break;
                        }
                        else
                            bn = 0;
                    }
                    else // we need more
                    {
                        result = this->checkSingle(*need, item);
                        if(result)
                        {
                            bn = 0;
                            break;
                        }
                        else
                        {
                            bn = qPow((1.0 - this->getSoftness()) * amountToGo / qMin(amount, requiredPicks), this->getSoftness());
                        }
                    }
                }
                else if(typeOfCheck == ">") //"at least"
                {
                    if(amountToGo <= 0) // we have enough, everybody is allowed in now
                    {
                        bn = 0;
                        break;
                    }
                    else // we need more
                    {
                        result = this->checkSingle(*need, item);
                        if(result)
                        {
                            bn = 0;
                            break;
                        }
                        else
                        {
                            bn = qPow((1.0 - this->getSoftness()) * amountToGo / qMin(amount, requiredPicks), this->getSoftness());
                        }
                    }
                }
            }
        }
    }

    if(badness)
    {
        *badness = bn;
    }
    return(result);
}

bool Criterion_ChecklistMinMax::checkSingle(const QString& need, const QObject* item) const
{
    bool result = true;
    QStringList haves = AbstractCriterionChecklist::getItemValues(item, &result);
    if(!result)
    {
        return(false);
    }

    QRegularExpression r(this->getAllowPartialMatch() ? need : QStringLiteral("^") + need + "$", QRegularExpression::CaseInsensitiveOption | QRegularExpression::DontCaptureOption);
    return(!haves.filter(r).isEmpty());
}

bool Criterion_ChecklistMinMax::load(const QMap<QString, QVariant>& information)
{
    if(AbstractCriterionChecklist::load(information))
    {
        this->blockSignals(true);
        this->setAmount(information.value(QStringLiteral(u"amount"), -1).toInt());
        this->setTypeOfCheck(information.value(QStringLiteral(u"listContains"), QStringLiteral(u">")).toString());
        if(typeOfCheck == ">=")
        {
            typeOfCheck = QStringLiteral(u">");
            amount --;
        }
        else if(typeOfCheck == "<=")
        {
            typeOfCheck = QStringLiteral(u"<");
            amount ++;
        }
        else if(typeOfCheck == "==")
        {
            typeOfCheck = QStringLiteral(u"=");
        }
        else if(typeOfCheck != "<" && typeOfCheck != ">" && typeOfCheck != "=") //invalid
        {
            qWarning("Invalid comparison operator in criterioin \"%s\".", qPrintable(this->getUsedAttribute()));
        }
        this->blockSignals(false);

        emit amountChanged(amount);
        emit typeOfCheckChanged(typeOfCheck);
        return(true);
    }
    return(false);
}

QMap<QString, QVariant> Criterion_ChecklistMinMax::save() const
{
    QMap<QString, QVariant> information = AbstractCriterionChecklist::save();
    QMetaEnum e = QMetaEnum::fromType<AbstractCriterion::CriterionType>();
    information.insert(QStringLiteral(u"type"), e.valueToKey(AbstractCriterion::CriterionType::CheckMinMax));
    information.insert(QStringLiteral(u"amount"), amount);
    information.insert(QStringLiteral(u"typeOfCheck"), typeOfCheck);
    return(information);
}

void Criterion_ChecklistMinMax::setAmount(int a)
{
    if(amount != a)
    {
        amount = a;
        emit amountChanged(amount);
    }
}

void Criterion_ChecklistMinMax::setTypeOfCheck(QString c)
{
    if(typeOfCheck != c)
    {
        typeOfCheck = c;
        emit typeOfCheckChanged(typeOfCheck);
    }
}

QString Criterion_ChecklistMinMax::toString(bool includeDisabled) const
{
    QString out;
    if(this->getEnabled() || includeDisabled)
    {
        out = QString("Criterion (Type): \"%1\" - Acting on property: \"%2\" - Softness: %3 - %4 %5 of \"%6\"")
                .arg(this->objectName(), 35)
                .arg(this->getUsedAttribute(), 15)
                .arg(this->getSoftness(), 3, 'f', 2)
                .arg(this->getTypeOfCheck())
                .arg(this->getAmount())
                .arg(this->getTargetValues().join(QChar(',')));
    }
    return(out);
}
