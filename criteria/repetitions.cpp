/*
 * Copyright 2020 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#include <QMetaEnum>

#include "repetitions.h"

bool Criterion_Repetitions::check(const QObject* item, const QList<QObject*>& previouslyPicked, int requiredPicks, double* badness)
{
    if(!this->getEnabled())
    {
        return(true);
    }

    bool result = true;
    double bn = MAXIMUM_BADNESS;
    int violations = 0;

    // check how many of the previouslyPicked items fulfull our criterion
    for(auto a = previouslyPicked.constBegin(); a != previouslyPicked.constEnd(); ++a)
    {
        if(this->getUsedAttribute().isEmpty())
        {
            if(item->objectName() == (*a)->objectName())
            {
                violations ++;
            }
        }
        else if(item->property(this->getUsedAttributePrimitive()) == (*a)->property(this->getUsedAttributePrimitive()))
        {
            violations ++;
        }
    }

    if (violations == 0)
    {
        bn = 0;
    }
    else
    {
        result = false;
        // the base rises linearly with softness and violations (see badness_checklist.gnu for a plot of this)
        bn = qPow((1.0 - this->getSoftness()) * (violations + 1) / requiredPicks, this->getSoftness()); // +1 to include ourselves as a "repetition"
    }

    if(badness)
    {
        *badness = bn;
    }
    return(result);
}

bool Criterion_Repetitions::load(const QVariantMap& information)
{
    return(AbstractCriterion::load(information));
}

QMap<QString, QVariant> Criterion_Repetitions::save() const
{
    QMap<QString, QVariant> information = AbstractCriterion::save();
    QMetaEnum e = QMetaEnum::fromType<AbstractCriterion::CriterionType>();
    information.insert(QStringLiteral(u"type"), e.valueToKey(AbstractCriterion::CriterionType::RepetitionBlock));
    return(information);
}
