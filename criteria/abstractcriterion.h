/*
 * Copyright 2020 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef ABSTRACTCRITERION_H
#define ABSTRACTCRITERION_H

#include <QObject>
#include <QMap>

#define MAXIMUM_BADNESS 1.0 // normalize badness into [0;1] such that 1 is the worst

class AbstractCriterion : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString identifier READ objectName WRITE setObjectName NOTIFY objectNameChanged) //objectName itself seems to be overriden by QML, hence this workaround
    Q_PROPERTY(QString usedAttribute READ getUsedAttribute WRITE setUsedAttribute NOTIFY usedAttributeChanged)
    Q_PROPERTY(bool enabled READ getEnabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(float softness READ getSoftness WRITE setSoftness NOTIFY softnessChanged)
public:
    enum CriterionType { Checklist, CheckMinMax, TotalBudget, SingleBudget, RepetitionBlock };
    Q_ENUM(CriterionType)

    /*!
     * \brief createCriterion Factory-method.
     * \param type Name of the type for which to create an instance.
     * \param parent Parent object.
     * \return The created criterion as an AbstractCriterion*.
     */
    static AbstractCriterion* createCriterion(CriterionType type, QObject* parent);
    static AbstractCriterion* createCriterion(const QString& type, QObject* parent);

    /*!
     * \brief check Check if criterion is fulfilled or how badly it is violated. Pure virtual.
     * \param item Item the check will be made for.
     * \param previouslyPicked List of previously picked items. If the check is successful, the given item might end up in that list.
     * \param requiredPicks Amount of items eventually needed.
     * \param badness A return value indicating the quality of the decision. The bigger the value, the worse the check went.
     * \return A bool indicating whether the check was successful. False means that the item shouldn't pass, but a badness-value should be provided anyway to allow an external decision.
     *         True means that the item should pass, but a non-zero badness-value might still be provided in case the caller ends up with multiple candidates.
     */
    virtual bool check(const QObject* item, const QList<QObject*>& previouslyPicked, int requiredPicks, double* badness) = 0;

    /*!
     * \brief prepareGlobal Allow the criterion to take a look at all existing items so it can adjust its expectations, if necessary. Only run once before generating any sets.
     * \param items List of all items to chose from.
     */
    virtual void prepareGlobal(const QList<QObject*>& items) { Q_UNUSED(items); }

    /*!
     * \brief preparePerList Allow the criterion to take a look at all existing items so it can adjust its expectations, if necessary. Run before a new set of items is generated.
     * \param items List of all items to chose from.
     */
    virtual void preparePerList(const QList<QObject*>& items) { Q_UNUSED(items); }

    /*!
     *  \brief load Set criterions properties from a map. Reimplement to handle custom properties.
     *  \param information A QMap<QString, QVariant> containing at least the keys "criterion" and "softness".
     *  \return A bool indicating whether loading was successful.
     */
    virtual bool load(const QMap<QString, QVariant>& information);
    virtual QMap<QString, QVariant> save() const;

    /*!
     *  \brief toString Set up a string containing information describing this criterion.
     *  \param includeDisabled Whether to give any information even if this criterion is not enabled.
     *  \return A QString.
     */
    virtual QString toString(bool includeDisabled = false) const;

    inline bool getEnabled() const { return(enabled); }
    inline const QString& getUsedAttribute() const { return(usedAttribute); }
    inline float getSoftness() const { return(softness); }
public slots:
    void setEnabled(bool e);
    void setUsedAttribute(const QString& n);
    void setSoftness(float s);
signals:
    void enabledChanged(bool);
    void usedAttributeChanged(const QString&);
    void objectNameChanged(const QString&);
    void softnessChanged(float);
protected:
    AbstractCriterion(QObject* parent) : QObject(parent), softness(0.0), enabled(true) { connect(this, &QObject::objectNameChanged, this, &AbstractCriterion::objectNameChanged); }
    inline const char* getUsedAttributePrimitive() const { return _cachedPrimitiveUsedAttribute; }
private:
    QString usedAttribute;
    const char* _cachedPrimitiveUsedAttribute; //name as char* to avoid constant conversions when calling QObject::property()
    float softness;
    bool enabled;
};

/*!
 * \brief The AbstractCriterionBudget class checks whether an item can be afforded given a fixed budget.
 */
class AbstractCriterionBudget : public AbstractCriterion
{
    Q_OBJECT
    Q_PROPERTY(double budget READ getBudget WRITE setBudget NOTIFY budgetChanged)
public:
    virtual bool load(const QMap<QString, QVariant>& information) override;
    virtual QMap<QString, QVariant> save() const override;

    double getCost(const QObject* item, bool* check) const;

    inline double getBudget() const { return budget; }
public slots:
    void setBudget(double b);
signals:
    void budgetChanged(double);
protected:
    AbstractCriterionBudget(QObject* parent) : AbstractCriterion(parent), budget(0.0) {}
private:
    double budget;
};

/*!
 * \brief Checks whether an item fulfills a set of criteria. Whether this means AND- or OR-combinations depends on the implementation.
 * NOTE 1: The softness should always be zero for this kind of criterion (since it has no influence).
 * NOTE 2: All comparisons are made case-insensitively.
 */
class AbstractCriterionChecklist : public AbstractCriterion
{
    Q_OBJECT
    Q_PROPERTY(QStringList targetValues READ getTargetValues WRITE setTargetValues NOTIFY targetValuesChanged)
    Q_PROPERTY(bool allowPartialMatch READ getAllowPartialMatch WRITE setAllowPartialMatch NOTIFY allowPartialMatchChanged)
public:
    virtual bool load(const QMap<QString, QVariant>& information) override;
    virtual QMap<QString, QVariant> save() const override;

    QStringList getItemValues(const QObject* item, bool* check) const;

    const QStringList& getTargetValues() const { return targetValues; }
    inline bool getAllowPartialMatch() const { return allowPartialMatch; }
public slots:
    void setTargetValues(const QString& v);  //will lead to a list with one entry
    void setTargetValues(QStringList v); //v needs to be modified
    void setAllowPartialMatch(bool m);
signals:
    void targetValuesChanged(QStringList);
    void allowPartialMatchChanged(bool);
protected:
    AbstractCriterionChecklist(QObject* parent) : AbstractCriterion(parent), allowPartialMatch(true) {}
private:
    QStringList targetValues;
    bool allowPartialMatch;
};

#endif // ABSTRACTCRITERION_H
