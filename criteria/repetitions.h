/*
 * Copyright 2020 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#ifndef REPETITIONS_H
#define REPETITIONS_H

#include "abstractcriterion.h"

/*!
 * \brief The Criterion_Repetitions class Suppresses repetitions of certain aspects by assigning a badness to items that occur more than once. check() will return false for repeated items.
 */
class Criterion_Repetitions : public AbstractCriterion
{
    Q_OBJECT
public:
    Criterion_Repetitions(QObject* parent) : AbstractCriterion(parent) { }

    virtual bool check(const QObject* item, const QList<QObject*>& previouslyPicked, int requiredPicks, double* badness) override;

    virtual bool load(const QVariantMap& information) override;
    virtual QMap<QString, QVariant> save() const override;
};

#endif // REPETITIONS_H
