/*
 * Copyright 2020 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#include <QMetaEnum>

#include "budget.h"

bool Criterion_TotalBudget::check(const QObject* item, const QList<QObject*>& previouslyPicked, int requiredPicks, double* badness)
{
    if(!this->getEnabled())
    {
        return(true);
    }

    bool result = true;
    double bn = MAXIMUM_BADNESS;
    double totalCost = 0;

    double budget = this->getBudget(requiredPicks);
    double deviation = this->getSoftness() * budget;
    double cost = AbstractCriterionBudget::getCost(item, &result);

    if(result) // true in case of an invalid property/value
    {
        for(auto i = previouslyPicked.constBegin(); i != previouslyPicked.constEnd(); ++i)
        {
            totalCost += AbstractCriterionBudget::getCost(*i, nullptr); //previously picked items must have been checked for property-validness already, hence pass NULLptr
        }
        cost = totalCost + cost;

        //go from comparison with lowest value (i.e. budget * minPercentage - deviation) to comparison with highest value (i.e. budget * maxPercentage + deviation)
        //deviation, minPercentage and maxPercentage usually do not have to be checked before being divided by since if any one was zero, the if/else spaghettis would take care of it before it could cause problems
        if(cost < budget * minPercentage - deviation) // too cheap
        {
            if(requiredPicks - previouslyPicked.count() == 1) //in case of the last item a definitive statement can be made
            {
                result = false; //item is simply too cheap at this point
            }
            else //otherwise no simple decision is possible yet; Wait it out.
            {
                bn = 0;
            }
        }
        else if(cost < budget * minPercentage) // slightly too cheap
        {
            // deviation cannot be 0, otherwise the if() before would have been triggered already
            if(requiredPicks - previouslyPicked.count() == 1) //in case of the last item a useful statement can be made
            {
                bn = (budget * minPercentage - cost) / deviation; //badness is element of [0,1] and rises linearly
            }
            else
            {
                bn = 0;
            }
        }
        else if(cost <= budget * maxPercentage) // perfect or slightly too expensive
        {
            bn = 0;
        }
        else if(cost <= budget * maxPercentage + deviation) // slightly too expensive
        {
            // deviation cannot be 0, otherwise the if() before would have been triggered already
            bn = (cost - budget * maxPercentage) / deviation; //badness is element of [0,1] and rises linearly
        }
        else // too expensive
        {
            result = false;
        }
    }

    if(badness)
    {
        *badness = bn;
    }
    return(result);
}

bool Criterion_TotalBudget::load(const QMap<QString, QVariant> &information)
{
    if(AbstractCriterionBudget::load(information))
    {
        this->blockSignals(true);
        this->setAutoMultiply(information.value(QStringLiteral(u"autoMultiply"), QStringLiteral("true")) == QLatin1String("true"));
        this->setMinPercentage(information.value(QStringLiteral(u"minPercentage"), 0.0).toDouble());
        this->setMaxPercentage(information.value(QStringLiteral(u"maxPercentage"), 1.0).toDouble());
        this->blockSignals(false);

        emit autoMultiplyChanged(autoMultiply);
        emit minPercentageChanged(minPercentage);
        emit maxPercentageChanged(maxPercentage);
        return(true);
    }
    return(false);
}

QMap<QString, QVariant> Criterion_TotalBudget::save() const
{
    QMap<QString, QVariant> information = AbstractCriterionBudget::save();
    QMetaEnum e = QMetaEnum::fromType<AbstractCriterion::CriterionType>();
    information.insert(QStringLiteral(u"type"), e.valueToKey(AbstractCriterion::CriterionType::TotalBudget));
    information.insert(QStringLiteral(u"autoMultiply"), this->getAutoMultiply() ? "true" : "false");
    information.insert(QStringLiteral(u"minPercentage"), this->getMinPercentage());
    information.insert(QStringLiteral(u"maxPercentage"), this->getMaxPercentage());
    return(information);
}

void Criterion_TotalBudget::prepareGlobal(const QList<QObject*>& items)
{
    AbstractCriterionBudget::prepareGlobal(items);
    for(auto i = items.constBegin(); i != items.constEnd(); ++i)
    {
        bool result = true;
        double cost = AbstractCriterionBudget::getCost(*i, &result);
        if(result)
        {
            _maxItemCost = qMax(cost, _maxItemCost);
        }
    }
}

void Criterion_TotalBudget::setAutoMultiply(bool m)
{
    if(autoMultiply != m)
    {
        autoMultiply = m;
        emit autoMultiplyChanged(autoMultiply);
    }
}

double Criterion_TotalBudget::getBudget(int elements) const
{
    return(AbstractCriterionBudget::getBudget() * (autoMultiply ? elements : 1));
}

void Criterion_TotalBudget::setMinPercentage(double p)
{
    if(!qFuzzyCompare(minPercentage, p))
    {
        minPercentage = p;
        emit minPercentageChanged(minPercentage);
    }
}

void Criterion_TotalBudget::setMaxPercentage(double p)
{
    if(!qFuzzyCompare(maxPercentage, p))
    {
        maxPercentage = p;
        emit maxPercentageChanged(maxPercentage);
    }
}

QString Criterion_TotalBudget::toString(bool includeDisabled) const
{
    QString out;
    if(this->getEnabled() || includeDisabled)
    {
        out = QString("Criterion (Type): \"%1\" - Acting on property: \"%2\" - Softness: %3 - %4-%5%6")
                .arg(this->objectName(), 35)
                .arg(this->getUsedAttribute(), 15)
                .arg(this->getSoftness(), 3, 'f', 2)
                .arg(this->getMinPercentage() * this->getBudget(1), 0, 'f', 2)
                .arg(this->getMaxPercentage() * this->getBudget(1), 0, 'f', 2)
                .arg(this->getAutoMultiply() ? QLatin1String("") : QLatin1String("(per element)"));
    }
    return(out);
}

//_________________________________________________________

bool Criterion_SingleBudget::check(const QObject* item, const QList<QObject*>& previouslyPicked, int requiredPicks, double* badness)
{
    Q_UNUSED(previouslyPicked)
    Q_UNUSED(requiredPicks)
    if(!this->getEnabled())
    {
        return(true);
    }

    double bn = MAXIMUM_BADNESS;
    double budget = this->getBudget();
    double deviation = this->getSoftness() * budget;

    bool result = true;
    double cost = AbstractCriterionBudget::getCost(item, &result);
    if(result)
    {
        //go from comparison with lowest value (i.e. budget - deviation) to comparison with highest value (i.e. budget + deviation)
        //deviation usually does not have to be checked before being divided by since if it was zero, the if/else spaghettis would take care of it before it could cause problems
        if(isLimit)
        {
            if(inverse) // lower limit
            {
                double temp = budget;
                budget = cost;
                cost = temp;
            }
            if(cost <= budget) //valid
            {
                bn = 0;
            }
            else if (qFuzzyIsNull(deviation) || budget + deviation < cost) // no deviation allowed or too cheap or too expensive
            {
                result = false;
            }
            else // slightly too cheap or too expensive
            {
                bn = qAbs(cost - budget) / deviation; //badness is element of [0,1] and rises linearly
            }
        }
        else
        {
            if(qFuzzyCompare(cost, budget)) // perfect
            {
                bn = 0;
            }
            else if (qFuzzyIsNull(deviation) || cost < budget - deviation || budget + deviation < cost) // no deviation allowed or too cheap or too expensive
            {
                result = false;
            }
            else // slightly too cheap or too expensive
            {
                bn = qAbs(cost - budget) / deviation; //badness is element of [0,1] and rises linearly
            }
        }
    }

    if(badness)
    {
        *badness = bn;
    }
    return(result);
}

bool Criterion_SingleBudget::load(const QMap<QString, QVariant>& information)
{
    if(AbstractCriterionBudget::load(information))
    {
        this->blockSignals(true);
        this->setInverse(information.value(QStringLiteral(u"inverse"), QStringLiteral(u"false")).toString() == QLatin1String("true"));
        this->setIsLimit(information.value(QStringLiteral(u"isLimit"), QStringLiteral(u"false")).toString() == QLatin1String("true"));
        this->blockSignals(false);

        emit inverseChanged(inverse);
        emit isLimitChanged(isLimit);
        return(true);
    }
    return(false);
}

QMap<QString, QVariant> Criterion_SingleBudget::save() const
{
    QMap<QString, QVariant> information = AbstractCriterionBudget::save();
    QMetaEnum e = QMetaEnum::fromType<AbstractCriterion::CriterionType>();
    information.insert(QStringLiteral(u"type"), e.valueToKey(AbstractCriterion::CriterionType::SingleBudget));
    information.insert(QStringLiteral(u"inverse"), inverse ? "true" : "false");
    information.insert(QStringLiteral(u"isLimit"), isLimit ? "true" : "false");
    return(information);
}

void Criterion_SingleBudget::setInverse(bool inv)
{
    if(inverse != inv)
    {
        inverse = inv;
        emit inverseChanged(inverse);
    }
}

void Criterion_SingleBudget::setIsLimit(bool l)
{
    if(isLimit != l)
    {
        isLimit = l;
        emit isLimitChanged(isLimit);
    }
}

QString Criterion_SingleBudget::toString(bool includeDisabled) const
{
    QString out;
    if(this->getEnabled() || includeDisabled)
    {
        out = QString("Criterion (Type): \"%1\" - Acting on property: \"%2\" - Softness: %3 - %4%5")
                .arg(this->objectName(), 35)
                .arg(this->getUsedAttribute(), 15)
                .arg(this->getSoftness(), 3, 'f', 2)
                .arg(this->getIsLimit() ? (this->getInverse() ? QStringLiteral(">") : QStringLiteral("<")) : QStringLiteral(u"~"))
                .arg(this->getBudget(), 0, 'f', 2);
    }
    return(out);
}
