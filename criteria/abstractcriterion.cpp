/*
 * Copyright 2020 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

#include <cfloat>
#include <QMetaEnum>
#include <QVariant>

#include "abstractcriterion.h"
#include "budget.h"
#include "checklist.h"
#include "repetitions.h"

AbstractCriterion *AbstractCriterion::createCriterion(const QString& type, QObject* parent)
{
    bool ok;
    QMetaEnum e = QMetaEnum::fromType<AbstractCriterion::CriterionType>();
    AbstractCriterion::CriterionType ct = (AbstractCriterion::CriterionType)e.keyToValue(type.toUtf8().constData(), &ok);
    if (ok)
        return(createCriterion(ct, parent));
    else
    {
        qWarning("Invalid type requested: %s", type.toUtf8().constData());
        return nullptr;
    }
}

AbstractCriterion *AbstractCriterion::createCriterion(CriterionType type, QObject* parent)
{
    if(type == CriterionType::Checklist)
        return new Criterion_Checklist(parent);
    else if(type == CriterionType::CheckMinMax)
        return new Criterion_ChecklistMinMax(parent);
    else if(type == CriterionType::TotalBudget)
        return new Criterion_TotalBudget(parent);
    else if(type == CriterionType::SingleBudget)
        return new Criterion_SingleBudget(parent);
    else if(type == CriterionType::RepetitionBlock)
        return new Criterion_Repetitions(parent);
    else
    {
        qWarning("Invalid type requested: %d", type);
        return nullptr;
    }
}

bool AbstractCriterion::load(const QMap<QString, QVariant>& information)
{
    if(information.contains(QStringLiteral(u"criterion")) && information.contains(QStringLiteral(u"softness")))
    {
        this->blockSignals(true);
        this->setUsedAttribute(information.value(QStringLiteral(u"criterion")).toString());
        this->setSoftness(information.value(QStringLiteral(u"softness")).toFloat());
        this->setEnabled(information.value(QStringLiteral(u"enabled"), QStringLiteral(u"false")) == QLatin1String("true"));
        this->setObjectName(information.value(QStringLiteral(u"identifier")).toString()); //full, unique identifier; no need to save it though, since it's going to be used to create QSettings-categories
        this->blockSignals(false);

        emit softnessChanged(softness);
        emit usedAttributeChanged(usedAttribute);
        emit enabledChanged(enabled);
        return(true);
    }
    return(false);
}

QMap<QString, QVariant> AbstractCriterion::save() const
{
    QMap<QString, QVariant> information;
    information.insert(QStringLiteral(u"enabled"), this->getEnabled() ? "true" : "false");
    information.insert(QStringLiteral(u"criterion"), this->getUsedAttribute());
    information.insert(QStringLiteral(u"softness"), QString::number(double(this->getSoftness()), 'g', 4)); //takes care of the precision, maximum 4 significant digits included
    return(information);
}

void AbstractCriterion::setEnabled(bool e)
{
    if(enabled != e)
    {
        enabled = e;
        emit enabledChanged(enabled);
    }
}

void AbstractCriterion::setUsedAttribute(const QString& n)
{
    if(usedAttribute != n)
    {
        usedAttribute = n;
        _cachedPrimitiveUsedAttribute = n.toUtf8().constData();
        emit usedAttributeChanged(usedAttribute);
    }
}

void AbstractCriterion::setSoftness(float s)
{
    if(softness != s)
    {
        softness = qBound(float(0.0), s, float(1.0)); //restrict softness to range [0.0;1.0]
        emit softnessChanged(softness);
    }
}

QString AbstractCriterion::toString(bool includeDisabled) const
{
    QString out;
    if(this->getEnabled() || includeDisabled)
    {
        out = QString("Criterion (Type): \"%1\" - Acting on property: \"%2\" - Softness: %3")
                .arg(this->objectName(), 35)
                .arg(this->getUsedAttribute(), 15)
                .arg(double(this->getSoftness()), 3, 'f', 2);
    }
    return(out);
}

//_________________________________________________________

bool AbstractCriterionBudget::load(const QMap<QString, QVariant>& information)
{
    if(AbstractCriterion::load(information) && information.contains(QStringLiteral("budget")))
    {
        this->blockSignals(true);
        this->setBudget(information.value(QStringLiteral(u"budget")).toDouble());
        this->blockSignals(false);

        emit budgetChanged(budget);
        return(true);
    }
    return(false);
}

QMap<QString, QVariant> AbstractCriterionBudget::save() const
{
    QMap<QString, QVariant> information = AbstractCriterion::save();
    information.insert(QStringLiteral(u"budget"), budget);
    return(information);
}

double AbstractCriterionBudget::getCost(const QObject* item, bool* check) const
{
    double cost = DBL_MAX;
    bool success = false;

    QVariant v = item->property(this->getUsedAttributePrimitive());
    if(v.isValid() && v.canConvert<double>())
    {
        double temp = v.toDouble(&success);
        if(success)
        {
            cost = temp;
        }
    }
    if(check)
        *check = success;
    return(cost);
}

void AbstractCriterionBudget::setBudget(double b)
{
    if(!qFuzzyCompare(b, budget))
    {
        budget = b;
        emit budgetChanged(budget);
    }
}

//_________________________________________________________

bool AbstractCriterionChecklist::load(const QMap<QString, QVariant>& information)
{
    if(AbstractCriterion::load(information) && information.contains(QStringLiteral("values")))
    {
        this->blockSignals(true);
        this->setTargetValues(information.value(QStringLiteral(u"values")).toStringList());
        this->setAllowPartialMatch(information.value(QStringLiteral(u"allowPartialMatch"), QStringLiteral(u"true")).toString() == "true");
        this->blockSignals(false);

        emit targetValuesChanged(targetValues);
        emit allowPartialMatchChanged(allowPartialMatch);
        return(true);
    }
    return(false);
}

QMap<QString, QVariant> AbstractCriterionChecklist::save() const
{
    QMap<QString, QVariant> information = AbstractCriterion::save();
    information.insert(QStringLiteral(u"values"), targetValues);
    information.insert(QStringLiteral(u"matchFixedStrings"), allowPartialMatch ? QStringLiteral(u"true") : QStringLiteral(u"false"));
    return(information);
}

QStringList AbstractCriterionChecklist::getItemValues(const QObject* item, bool* check) const
{
    QStringList out;
    QString temp;
    bool success = true;

    QVariant v = item->property(this->getUsedAttributePrimitive());
    if(v.canConvert<QStringList>() && !(out = v.toStringList()).isEmpty())
    {
        // nothing left to be done
    }
    else if(v.canConvert<QString>() && !(temp = v.toString()).isNull() && !(out = QStringList(temp)).isEmpty())
    {
        // nothing left to be done
    }
    else
    {
        success = false;
    }
    if(check)
        *check = success;
    return(out);
}

void AbstractCriterionChecklist::setTargetValues(const QString& v)
{
    if(targetValues.count(v) != targetValues.count())
    {
        targetValues.clear();
        targetValues.append(v);

        emit targetValuesChanged(targetValues);
    }
}

void AbstractCriterionChecklist::setTargetValues(QStringList v)
{
    v.removeDuplicates();
    v.sort();

    QStringList vo = QStringList(targetValues);
    vo.removeDuplicates();
    vo.sort();

    if(vo != v)
    {
        targetValues.clear();
        targetValues.append(v);

        emit targetValuesChanged(targetValues);
    }
}

void AbstractCriterionChecklist::setAllowPartialMatch(bool m)
{
    if(allowPartialMatch != m)
    {
        allowPartialMatch = m;
        emit allowPartialMatchChanged(allowPartialMatch);
    }
}
