/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

import QtQuick 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12

Rectangle {
    id: settingsChecklist

    ColumnLayout {
        x: -189
        y: -280
        //anchors.fill: parent
        width: 300
        height: 500
        spacing: 2

        TextEdit {
            id: critFullNameChecklist
            text: qsTr("identifier")
            font.pixelSize: 12
            Layout.minimumHeight: 30
            Layout.preferredHeight: 30
        }

        TextEdit {
            id: critTargetPropertyChecklist
            text: qsTr("property")
            font.pixelSize: 12
            Layout.minimumHeight: 30
            Layout.preferredHeight: 30
        }

        TextEdit {
            id: valuesChecklist
            Layout.minimumHeight: 30
            Layout.preferredHeight: 30
        }

        CheckBox {
            id: matchFixedStringChecklist
            checked: false
            text: qsTr("Match Fixed String")
            Layout.minimumHeight: 30
            Layout.preferredHeight: 30
        }

        CheckBox {
            id: inverseChecklist
            checked: false
            text: qsTr("Inverse")
            Layout.minimumHeight: 30
            Layout.preferredHeight: 30
        }
    }
}

