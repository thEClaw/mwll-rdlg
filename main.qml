/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

import QtQuick 2.4
import QtQuick.Controls 2.15
//import QtQuick.Layouts 1.1

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 1024
    height: 700
    onClosing: handler.cancelGeneration = true
    title: "MWLL-RDLG v1.5 - Random Droplist Generator"

    menuBar: MenuBar {
        id: menu
        Menu {
            id: fileMenu
            title: qsTr("File")
            MenuItem {
                text: qsTr("Reload")
                shortcut: qsTr("Ctrl+R")
                onTriggered: handler.reload();
            }
            MenuItem {
                text: qsTr("Save Assets")
                onTriggered: handler.saveAssets();
            }
            MenuItem {
                text: qsTr("Save Criteria")
                shortcut: qsTr("Ctrl+Shift+S")
                onTriggered: handler.saveCriteria();
            }
            MenuItem {
                text: qsTr("Save Settings")
                shortcut: qsTr("Ctrl+S")
                onTriggered: handler.saveSettings();
            }
            MenuSeparator { }
            MenuItem {
                text: qsTr("Exit")
                shortcut: qsTr("Ctrl+Q")
                onTriggered: Qt.quit();
            }
        }
    }

    statusBar: MyStatusBar {
    }

    ButtonGenerate {
        id: buttonGenerate
    }

    SettingContainer {
        id: settingContainer
    }

    Connections {
            target: handler
            onAllListsDone: {
                buttonGenerate.state = "idle"
            }
            onListDone: {
                if(droplist.text == "")
                    droplist.text = list
                else
                    droplist.append("\n" + list)
                droplist.state = "newText"
            }
    }

    SplitView {
        id: splitView
        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.top: buttonGenerate.bottom
        anchors.topMargin: 5

        orientation: Qt.Horizontal

        MyTextArea {
            id: droplist
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Rectangle {
            id: listViewContainer
            Layout.minimumHeight: 200
            Layout.minimumWidth: 35
            Layout.maximumWidth: {
                if(splitView.orientation === Qt.Horizontal)
                    300;
                else
                    -1;
            }
            Layout.maximumHeight: {
                if(splitView.orientation === Qt.Horizontal)
                    -1;
                else
                    400;
            }
            width: criteriaView.width

            ListView {
                id: criteriaView
                width: 300
                anchors.bottomMargin: 8
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: addCriterionButton.top

                model: handler.qmlCriteria

                delegate: Rectangle {
                    state: "unselected"
                    id: wrapper
                    width: parent.width

                    CheckBox {
                        id: checkCriterion
                        anchors.left: parent.left
                        anchors.leftMargin: 5
                        anchors.top: parent.top
                        height: 30
                        checked: model.enabled
                        onClicked: model.enabled = checked ? true : false

                        style: CheckBoxStyle {
                            indicator: Rectangle {
                                implicitHeight: 26
                                implicitWidth: 26
                                color: "transparent"
                                border.width: 1
                                border.color: "#AAA"
                                anchors.margins: 2
                                Rectangle {
                                    visible: control.checked
                                    color: "#AAA"
                                    anchors.margins: 3
                                    anchors.fill: parent
                                }
                            }
                        }
                    }

                    Rectangle {
                        id: rowContainer
                        anchors.left: checkCriterion.right
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.rightMargin: 5
                        Text {
                            id: textFull
                            height: 20
                            anchors.left: parent.left
                            anchors.top: parent.top
                            anchors.bottom: textType.top
                            anchors.right: parent.right
                            anchors.rightMargin: 30
                            text: model.fullName
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                        }
                        Text {
                            id: textType
                            height: 10
                            anchors.left: parent.left
                            anchors.right: parent.right
                            anchors.top: textFull.bottom
                            anchors.rightMargin: 30
                            text: model.name
                            verticalAlignment: Text.AlignVCenter
                            font.italic: true
                            font.pointSize: 7
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                criteriaView.currentIndex = index
                            }
                        }

                        Rectangle {
                            id: deleteCrit
                            anchors.top: parent.top
                            anchors.topMargin: 9
                            anchors.right: parent.right
                            anchors.rightMargin: 5
                            height: 12
                            width: 12
                            color: "transparent"

                            Image {
                                anchors.fill: parent
                                source: "icons/clear_grey.png"
                            }

                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    if(criteriaView.currentIndex >= 0)
                                        handler.removeCriterion(criteriaView.currentIndex)
                                }
                            }
                        }

                        SettingsLayout {
                            id: settings
                        }
                    }

                    states: [
                        State {
                            name: "unselected"
                            when: !wrapper.ListView.isCurrentItem
                            PropertyChanges { target: wrapper; height: 30 }
                            PropertyChanges { target: wrapper; color: "white" }
                            PropertyChanges { target: rowContainer; height: 30 }
                            PropertyChanges { target: rowContainer; color: "white" }
                            PropertyChanges { target: textFull; color: "black" }
                            PropertyChanges { target: textType; color: "black" }
                            PropertyChanges { target: settings; visible: false }
                            PropertyChanges { target: deleteCrit; visible: false }
                        },
                        State {
                            name: "selected"
                            when: wrapper.ListView.isCurrentItem
                            PropertyChanges { target: wrapper; color: "#3e66f7"}
                            PropertyChanges { target: rowContainer; color: "#3e66f7"}
                            PropertyChanges { target: textFull; color: "white"}
                            PropertyChanges { target: textType; color: "white"}
                            PropertyChanges { target: settings; visible: true}
                            PropertyChanges { target: wrapper; height: settings.height + textFull.height + textType.height + 10}
                            PropertyChanges { target: rowContainer; height: settings.height + textFull.height + textType.height + 10}
                            PropertyChanges { target: deleteCrit; visible: true }
                        }
                    ]

                    transitions: Transition {
                        from: "unselected"
                        to: "selected"
                        PropertyAnimation {
                            targets: [wrapper, rowContainer]
                            properties: "height"
                            duration: 100
                            easing.type: Easing.OutQuad
                        }
                        NumberAnimation {
                            targets: settings
                            properties: "opacity"
                            duration: 300
                            easing.type: Easing.OutQuad
                            from: 0
                            to: 1
                        }
                    }
                }
            }

            ComboBox {
                id: addCriterion
                anchors.right: addCriterionButton.left
                anchors.rightMargin: 2
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 5

                model: ListModel {
                    id: newCriteria
                    ListElement { text: qsTr("<New>")     }
                    ListElement { text: "Checklist"       }
                    ListElement { text: "CheckMinMax"     }
                    ListElement { text: "TotalBudget"     }
                    ListElement { text: "SingleBudget"    }
                    ListElement { text: "RepetitionBlock" }
                }
            }

            Button {
                id: addCriterionButton
                anchors.right: criteriaView.right
                anchors.rightMargin: 5
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 5
                text: qsTr("+")
                width: 20

                onClicked: {
                    var index = addCriterion.currentIndex
                    if(index > 0) {
                        var t = addCriterion.currentText
                        handler.appendCriterion(t)
                        criteriaView.currentIndex = criteriaView.count - 1
                    }
                }
            }
        }
    }
}
