/*
 * Copyright 2015 S. Ehlers
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 */

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.3

ColumnLayout {
    id: settings
    function checkCriterionType(prop, m)
    {
        return prop in m;
    }

    spacing: 2
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.top: textType.bottom
    anchors.topMargin: settings.spacing

    TextField { //AbstractCriterion
        id: identifier
        Layout.fillWidth: true
        readOnly: false
        placeholderText: qsTr("Unique Name")
        text: parent.checkCriterionType("fullName", model) ? model.fullName : ""
        onTextChanged: model.fullName = text
        visible: parent.checkCriterionType("fullName", model)
    }

    TextField { //AbstractCriterion
        id: prop
        Layout.fillWidth: true
        readOnly: false
        placeholderText: qsTr("Attribute")
        text: parent.checkCriterionType("name", model) ? model.name : ""
        onTextChanged: model.name = text
        visible: parent.checkCriterionType("name", model)
    }

    SpinBox { //AbstractCriterion
        id: softness
        suffix: qsTr(" Softness")
        Layout.fillWidth: true
        decimals: 2
        stepSize: 0.01
        minimumValue: 0.0
        maximumValue: 1.0
        value: model.softness
        visible: parent.checkCriterionType("softness", model) && !parent.checkCriterionType("values", model)
        onValueChanged: model.softness = value
    }

    SpinBox { //AbstractCriterionBudget
        id: budget
        suffix: qsTr(" Budget")
        Layout.fillWidth: true
        decimals: 2
        stepSize: 1
        minimumValue: -1000000000
        maximumValue: 1000000000
        value: parent.checkCriterionType("budget", model) ? model.budget : 0
        onValueChanged: {
            model.budget = value
            stepSize = (value <= 10 ? 0.25 : (value <= 100 ? 1 : (value <= 500 ? 5 : value <= 1000 ? 10 : (value <= 10000 ? 100 : 1000))))
        }
        visible: parent.checkCriterionType("budget", model)
    }

    SpinBox { //Criterion_TotalBudget
        id: minPercentage
        prefix: qsTr("At least ")
        suffix: qsTr(" %")
        Layout.fillWidth: true
        decimals: 1
        minimumValue: 0.0
        maximumValue: 99.8

        value: parent.checkCriterionType("minPercentage", model) ? model.minPercentage * 100 : 0
        onValueChanged: {
            model.minPercentage = value / 100
        }
        visible: parent.checkCriterionType("minPercentage", model)
    }

    SpinBox { //Criterion_TotalBudget
        id: maxPercentage
        prefix: qsTr("At most ")
        suffix: qsTr(" %")
        Layout.fillWidth: true
        decimals: 1
        minimumValue: 0.2
        maximumValue: 100.0
        value: parent.checkCriterionType("maxPercentage", model) ? model.maxPercentage * 100 : 0
        onValueChanged: {
            model.maxPercentage = value / 100
            minPercentage.maximumValue = value - 0.2
        }
        visible: parent.checkCriterionType("maxPercentage", model)
    }

    TextField { //AbstractCriterionChecklist
        id: values
        Layout.fillWidth: true
        readOnly: false
        placeholderText: qsTr("Values")
        text: parent.checkCriterionType("values", model) ? model.values.join(", ") : ""
        onEditingFinished: {
            var t = text
            t = t.trim()
            t = t.replace(" , ", "|").replace(" ,", "|").replace(", ", "|").replace(",", "|")
            t = t.replace(" | ", "|").replace(" |", "|").replace("| ", "|")
            model.values = t.split("|")
        }
        visible: parent.checkCriterionType("values", model)
    }

    CheckBox { //AbstractCriterionChecklist
        id: matchFixedString
        text: qsTr("Match Fixed String")
        Layout.fillWidth: true
        checked: parent.checkCriterionType("matchFixedString", model) ? model.matchFixedString : false
        onClicked: model.matchFixedString = checked ? true : false
        visible: parent.checkCriterionType("matchFixedString", model)
        style: CheckBoxStyle {
            background: Rectangle {
                color: "transparent"
            }
            label: Label {
                color: "white"
                text: control.text
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }
        }
    }

    CheckBox { //Criterion_TotalBudget
        id: autoMultiply
        text: qsTr("Auto-Multiply")
        Layout.fillWidth: true
        checked: parent.checkCriterionType("autoMultiply", model) ? model.autoMultiply : false
        onClicked: model.autoMultiply = checked ? true : false
        visible: parent.checkCriterionType("autoMultiply", model)
        style: CheckBoxStyle {
            background: Rectangle {
                color: "transparent"
            }
            label: Label {
                color: "white"
                text: control.text
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }
        }
    }

    CheckBox { //Criterion_SingleBudget, Criterion_Checklist
        id: inverse
        text: qsTr("Inverse")
        Layout.fillWidth: true
        checked: parent.checkCriterionType("inverse", model) ? model.inverse : false
        onClicked: model.inverse = checked ? true : false
        visible: parent.checkCriterionType("inverse", model)
        style: CheckBoxStyle {
            background: Rectangle {
                color: "transparent"
            }
            label: Label {
                color: "white"
                text: control.text
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }
        }
    }

    CheckBox { //Criterion_SingleBudget
        id: isLimit
        text: qsTr("Is Limit")
        Layout.fillWidth: true
        checked: parent.checkCriterionType("isLimit", model) ? model.isLimit : false
        onClicked: model.isLimit = checked ? true : false
        visible: parent.checkCriterionType("isLimit", model)
        style: CheckBoxStyle {
            background: Rectangle {
                color: "transparent"
            }
            label: Label {
                color: "white"
                text: control.text
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }
        }
    }

    SpinBox { //Criterion_ChecklistMinMax
        id: amount
        prefix: qsTr("Amount: ")
        Layout.fillWidth: true
        decimals: 0
        minimumValue: 0
        value: parent.checkCriterionType("amount", model) ? model.amount : 0
        onValueChanged: model.amount = value
        visible: parent.checkCriterionType("amount", model)
    }

    GroupBox {
        id: listContains
        flat: true
        Layout.fillWidth: true
        visible: parent.checkCriterionType("listContains", model)
        Row {
            spacing: 5
            ExclusiveGroup { id: listContainsGroup }
            RadioButton {
                id: listContainsMore
                text: qsTr("more than")
                exclusiveGroup: listContainsGroup
                checked: settings.checkCriterionType("listContains", model) ? model.listContains === ">" : false
                onCheckedChanged: {
                    if(checked)
                        model.listContains = ">"
                }

                style: RadioButtonStyle {
                    background: Rectangle {
                        color: "transparent"
                    }
                    label: Label {
                        color: "white"
                        text: control.text
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                    }
                }
            }
            RadioButton {
                id: listContainsExactly
                text: qsTr("exactly")
                exclusiveGroup: listContainsGroup
                checked: settings.checkCriterionType("listContains", model) ? model.listContains === "=" : false
                onCheckedChanged: {
                    if(checked)
                        model.listContains = "="
                }

                style: RadioButtonStyle {
                    background: Rectangle {
                        color: "transparent"
                    }
                    label: Label {
                        color: "white"
                        text: control.text
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                    }
                }
            }
            RadioButton {
                id: listContainsLess
                text: qsTr("less than")
                exclusiveGroup: listContainsGroup
                checked: settings.checkCriterionType("listContains", model) ? model.listContains === "<" : false
                onCheckedChanged: {
                    if(checked)
                        model.listContains = "<"
                }

                style: RadioButtonStyle {
                    background: Rectangle {
                        color: "transparent"
                    }
                    label: Label {
                        color: "white"
                        text: control.text
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                    }
                }
            }
        }
    }
}

