# README #

*MWLL-RDLG* is short for "MechWarrior Living Legends Random Droplist Generator". [MechWarrior Living Legends](http://www.mechlivinglegends.net/) is (respectively was) a total modification for *Crysis Wars* and provides the best (to date, including the 2013 MechWarrior cashcow MWO) combined arms multiplayer Battletech experience available.

A compiled Windows-version of *MWLL-RDLG* can be found in the [MWLL forums](http://forum.mechlivinglegends.net/index.php/topic,20510).

Technically the program could be made to run under any major operating system; with some redesigning of the UI versions for mobile operating systems would be decently easy to create, too.

### I found a bug or have a suggestion! ###

Please head over to the Issues-section and create a new issue. Since I consider development finished as of now, I cannot guarantee to start work on the software again, though.

### Contribution ###

If you do not only have great ideas but also the skills to implement them, please fork the repository and start sending in pull requests. I would be happy to review and consider merging them.

Even though I think my work here is done, I would like to be asked should you want to fork and develop your own version of *MWLL-RDLG*.